"use strict";
const $ = window.$ = window.jQuery = require("jquery");
require("owl.carousel/dist/owl.carousel");
require("jquery-lazy/jquery.lazy");
require("jquery-mask-plugin");
require("jquery-confirm")(jQuery);
require("./lib/jquery.oembed.js");
require('./components/header.js').Init();
// require('./components/infiniteScroll.js').Init();



const AOS = require("aos");

$('body').append(`<div class="site-loading"></div>`);

$(window).on('loading', function(){
  $('html, body').addClass('is-loading');
});

$(window).on('unloading', function(){
  $('html, body').removeClass('is-loading');
});


// window.addEventListener('beforeunload', () => $(window).trigger("unloading") );
// window.addEventListener('load', () => $(window).trigger("loading") );

$('.lazy').lazy({
  effect: "fadeIn",
  effectTime: 'fast',
  threshold: 0
});

AOS.init({
  // disable : ($(window).width() < 992),
  once: false
});

$('.article style').remove();
$('.article font').attr("size", "");


var checkHash = function(){
  var hash = window.location.hash || null;

  if( hash && hash.indexOf('#') == -1 ){ hash = '#'+hash; }

  if( hash && $(hash).length ){
    window.scrollTo(0, 0);
    $('html, body').animate({
      scrollTop: ($(hash).offset().top - $('#header').height())
    }, 500, 'swing', null)
  }
}

window.onhashchange = checkHash;

setTimeout(() => {
  checkHash();
}, 300);

$('.social-share a').on('click', (e) => {
  var net = $(e.currentTarget).attr("data-network") || null;
  var url = encodeURIComponent(document.URL || window.location.href);
  var title = encodeURIComponent(document.title+" ");

  switch( net ){
    case "facebook":
      window.open("https://www.facebook.com/sharer/sharer.php?u="+url);
      break;
    case "linkedin":
      window.open("https://www.linkedin.com/shareArticle?mini=true&url="+url+"&title="+title+"&summary=&source=");
      break;
    case "whatsapp":
      window.open("https://api.whatsapp.com/send?text="+title+''+url);
      break;
    case "twitter":
      window.open("https://twitter.com/home?status="+title+''+url);
      break;
  }
});
