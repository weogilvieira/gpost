"use strict";

var $ = window.jQuery = require("jquery")

module.exports = (function($){
  var obj = {};

  obj.Attach = () => {


    $('.main-mobile-nav').on('swiperight', () => obj.$btnMenu.trigger('click') );

    obj.$btnMenu.on('click touch', (e) => {
      $('body').toggleClass('main-mobile-nav-open');
      obj.$btnMenu.find('.fa').toggleClass('fa-bars fa-times')
    });

    obj.$shadow.on('click touch', () => obj.$btnMenu.trigger('click') );

    obj.$toggleSub.on('click', function(e) {
      $(this).toggleClass('active')
    });

    obj.$window.on('resize', () => {
      if( obj.$window.width() > 992 ){
        $('body').removeClass('main-mobile-nav-open');
        obj.$btnMenu.find('.fa').removeClass('fa-times').addClass('fa-bars')
      }  
    });

  };

  obj.Init = () => {
    obj.$container = $('#header');
    if( !obj.$container.length ){ return; }
    obj.$btnMenu = obj.$container.find('.main-header__button-menu');
    obj.$toggleSub = $('.main-mobile-nav .has-sub');
    obj.$shadow = $('.main-nav-mobile__shadow');

    obj.$window = $(window);

    obj.Attach();
  };

  return obj;
})($ || jQuery)
