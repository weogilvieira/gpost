"use strict";

var $ = window.jQuery = require("jquery");
var GoogleMapsLoader = require('google-maps');
var geocomplete = require("geocomplete");

GoogleMapsLoader.KEY = "AIzaSyA6sjgcMEtEQ8p58fCyjbQMz4TTOloZRLY";
GoogleMapsLoader.LIBRARIES = ['geometry', 'places'];
GoogleMapsLoader.LANGUAGE = 'pt-br';
GoogleMapsLoader.VERSION = '3.14';

GoogleMapsLoader.load(function(google) {
  window.google = google;
});

module.exports = (function($){
  var obj = {};
      obj.filter = [];
      obj.user_location = null;
      obj.MAP = null;

  obj.RunFilter = function(){
    obj.$items.map((key, el) => {
      if( obj.filter.indexOf($(el).attr("data-type")) !== -1 ) {
        $(el).addClass('-inactive');
      } else {
        $(el).removeClass('-inactive');
      }
    });
  };

  obj.getDistance = function(a, b, c, d, e, f){
    var g = 6371,
        h = (c - a) * Math.PI / 180,
        i = (d - b) * Math.PI / 180,
        j = Math.sin(h / 2) * Math.sin(h / 2) + Math.cos(a * Math.PI / 180) * Math.cos(c * Math.PI / 180) * Math.sin(i / 2) * Math.sin(i / 2),
        k = 2 * Math.atan2(Math.sqrt(j), Math.sqrt(1 - j)),
        l = g * k,
        m = l <= e ? 1 : 0;

        "function" == typeof f && f(l, m);
  }

  obj.findStore = function() {
      obj.$container.addClass('only-nearby');
      $(obj.$items[a]).find('.distance').text("");

      var a = 0,
          c = {};
      for (a = 0; a < obj.$items.length; a++){ 

        obj.getDistance(obj.user_location.lat, obj.user_location.lng, Number($(obj.$items[a]).attr('data-lat')), Number($(obj.$items[a]).attr('data-lng')), 30, function(d, e) {

          $(obj.$items[a]).find('.distance').text(`Distância: ${parseInt(d)}km`);

          if( e ){
            c = $(obj.$items[a]);
            $(obj.$items[a]).addClass('-nearby');
          };
        });

      }//for       
  }

  obj.Attach = function() {

    obj.$btnReset.on('click', function(e){
      obj.$formSearch.find('input[type=text]').val("");
      obj.$container.removeClass('only-nearby');
      obj.$items.removeClass('-nearby');
      obj.$btnFilter.removeClass('-off');
      obj.RunFilter();
      
      e.preventDefault();
    });

    obj.$items.on('click', function(e) {
      obj.$items.removeClass('-active');
      $(this).addClass('-active');

      e.preventDefault();
      let lat = Number($(this).attr("data-lat"));
      let lng = Number($(this).attr("data-lng"));

      if( $(window).width() < 992 ) { 
        window.open(`https://maps.google.com/?q=${lat},${lng}`);
        return; 
      }

      let location = new google.maps.LatLng(lat, lng);
      obj.MAP.setCenter(location);
      obj.MAP.setZoom(18);
      return;
    })

    obj.$btnFilter.on('click', function(e) {
      e.preventDefault();
      $(this).toggleClass('-off');
      obj.filter = [];
      obj.$btnFilter.filter('.-off').map( (a, el) => {
        obj.filter.push($(el).attr('data-type'));
      });
      obj.RunFilter();
    });


    obj.$formSearch.on('submit', function(e) {
      obj.findStore();
      e.preventDefault();
      return;
    });

    GoogleMapsLoader.load(function(google) {
      window.google = google;
      obj.$formSearch.find('input[type=text]').geocomplete();
      obj.$container.removeClass('-loading');

      if( $(window).width() < 992 ) { return; }

      obj.MAP = new google.maps.Map(document.getElementById('map'), {
        zoom: 15
      });

      navigator.geolocation.getCurrentPosition(function(position) {
        var initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        obj.MAP.setCenter(initialLocation);
        obj.MAP.setZoom(15);
      }, function(positionError) {
        console.error(positionError)
      });


      obj.$items.map((key, el) => {

        let location = new google.maps.LatLng(Number($(el).attr("data-lat")), Number($(el).attr("data-lng")));

        var marker = new google.maps.Marker({
          position: location,
          map: obj.MAP,
          title: $(el).find(".title").text()
        });

      });

    });

    obj.$formSearch.find('input[type=text]').on('geocode:result', function(event, res) {
      obj.user_location = {
        lat: res.geometry.location.lat(),
        lng: res.geometry.location.lng()
      }
    })
  };

  obj.Init = () => {
    obj.$container = $('.map-explorer');
    if( !obj.$container.length ){ return; }
    obj.$container.addClass('-loading');

    obj.$items = obj.$container.find('.list-wrap .list ul li');
    obj.$btnFilter = obj.$container.find('.btn-filter');
    obj.$formSearch = obj.$container.find('.form-search');
    obj.$btnReset = obj.$formSearch.find('.btn-reset');

    obj.Attach();
  };

  return obj;
})($ || jQuery)
