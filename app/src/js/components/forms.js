"use strict";

var $ = window.jQuery = require("jquery")

module.exports = (function($){
  var obj = {};

  obj.Send = function(path, data, cb) {
    
    $.ajax({
      url: path,
      type: 'POST',
      data: data
    }).done(function(res) {
      cb(res);
    }).fail(() => {
      cb(false);
    });

  };


  obj.Attach = () => {

    obj.$container.on('submit', function(e) {
      e.preventDefault();
      var $el = $(this);
      var serializeData = $(this).serializeArray();
      var path = $(this).attr("action");
      var data = {};

      $el.addClass('is-loading');
      $el.find('input, textarea, button, select').attr("disabled", "disabled");

      $.map(serializeData, function(n, i){
        data[n['name']] = n['value'];
      });

      obj.Send(path, data, function(res){

        $el.find('input, textarea, button, select').removeAttr("disabled", "disabled");
        
        $el.removeClass('is-loading');

        if(!res) {
          alert("Erro ao enviar dados, tente novamente.");
          return;
        }

        if(!res.errors && res.message) {
          alert(res.message);
          $el.reset();
          return;
        }

      });


      return false;
    });


  };

  obj.Init = () => {
    obj.$container = $('.form-ajax');
    if( !obj.$container.length ){ return; }
    obj.Attach();
  };

  return obj;
})($ || jQuery)
