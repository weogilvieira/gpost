"use strict";

var $ = window.jQuery = require("jquery")

module.exports = (function($){
  var obj = {};

  obj.IS_LOADING = false;

  obj.LoadMore = function(){
    if(obj.IS_LOADING){ return; }

    obj.current++;
    
    if( obj.current > obj.pages ){ return; }

    obj.$container.trigger(':loading');

    $.ajax({
      url: '/',
      type: 'get',
      dataType: 'text',
      data: { 
        page: obj.current,
        type: "infinite"
      },
    })
    .done(function(res) {
      obj.$container.find('.row-items').append(res);

      console.log( obj.$container.find(".lazy:not([style])") );
    
      obj.$container.find(".lazy:not([style])").lazy({
        chainable: false,
        effect: "fadeIn",
        effectTime: 'fast',
        threshold: 0
      });


    })
    .fail(function(res) {
      console.log("error", res);
    })
    .always(function() {
      obj.$container.trigger(':unload');
    });
    
  };

  obj.CheckPositionToLoadMore = function() {
    if( !obj.$loadingPointer.length > 0 ){ return; }

    if( $(window).scrollTop() <= ($(document).height() - ($("#footer").outerHeight()+100)) - $(window).height()){
      obj.LoadMore();
    }
  }

  obj.Attach = () => {
    
    obj.$btnLoadMore.on('click', function(e) {
      e.preventDefault();
      return obj.LoadMore();
    });

    obj.$container.on(":loading", function(e) {
      obj.IS_LOADING = true;
      obj.$btnLoadMore.find("span").html("<i class=\"fa fa-refresh fa-spin\"></i> <span>Carregando...</span>")
    });

    obj.$container.on(":unload", function(e) {
      obj.IS_LOADING = false;
      obj.$btnLoadMore.find("span").html("<span>Carregar mais <i class=\"fa fa-plus\"></i><span>");

      if( obj.current >= obj.pages ){
        obj.$loadingPointer.remove();
      };
    });

    $(document).on("scroll", function(){
      obj.CheckPositionToLoadMore();
    });


  };

  obj.Init = () => {
    obj.$container = $('.home-infinity-scroll');
    if( !obj.$container.length ){ return; }
    obj.$btnLoadMore = obj.$container.find('.btn-loadmore');
    obj.$loadingPointer = obj.$container.find('.home-loading-pointer');

    obj.pages = Number(obj.$container.attr("data-pages"));
    obj.current = Number(obj.$container.attr("data-current"));

    obj.Attach();
    obj.CheckPositionToLoadMore();
  };

  return obj;
})($ || jQuery)
