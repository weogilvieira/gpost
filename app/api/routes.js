const express = require('express');
const router = express.Router();

var Auth = require("./controllers/Auth.Admin.Controller");
var User = require("./controllers/User.Admin.Controller");
var Account = require("./controllers/Account.Admin.Controller");
var Pages = require("./controllers/Pages.Admin.Controller");
var PostType = require("./controllers/PostType.Admin.Controller");
var Posts = require("./controllers/Posts.Admin.Controller");
var Site = require("./controllers/Site.Admin.Controller");
var Media = require("./controllers/Media.Admin.Controller");
var Forms = require("./controllers/Forms.Admin.Controller");
var Custom = require("./controllers/Custom.Admin.Controller");
var Taxonomy = require("./controllers/Taxonomy.Admin.Controller");
var Relationship = require("./controllers/Relationship.Admin.Controller");

const mcache = require('memory-cache');
var cache = (duration) => {
  return (req, res, next) => {
    let key = '__express__' + req.originalUrl || req.url
    let cachedBody = mcache.get(key)
    if (cachedBody) {
      res.send(cachedBody)
      return
    } else {
      res.sendResponse = res.send
      res.send = (body) => {
        mcache.put(key, body, duration * 1000);
        res.sendResponse(body)
      }
      next()
    }
  }
}


module.exports = (app) => {
  
  router.get('/', (req, res) => {
    res.status(200).send({});
  });

  router.post('/login', Auth.index);
  router.get('/logout', Auth.logout);

  //ACCOUNT
  router.route('/me')
    .get(Auth.checkToken, Account.index)
    .put(Auth.checkToken, Account.update);

  router.put('/me/password', Auth.checkToken, Account.updatePass)

  //SITE
  router.route('/site')
    .get(Site.index);

  //SETTINGS
  router.route('/site/key')
    .get(Auth.checkToken, Site.keys)
    .post(Auth.checkToken, Site.post);

  router.route('/site/key/:id')
    .get(Auth.checkToken, Site.get)
    .put(Auth.checkToken, Site.put)
    .delete(Auth.checkToken, Site.delete);

  //PAGES
  router.route('/pages')
    .get(Auth.checkToken, Pages.index)
    .post(Auth.checkToken, Pages.post);

  router.route('/pages/templates')
    .get(Auth.checkToken, Pages.getTemplates)

  router.route('/pages/hooks')
    .get(Auth.checkToken, Pages.getHooks)

  router.route('/pages/:id')
    .get(Auth.checkToken, Pages.get)
    .put(Auth.checkToken, Pages.put)
    .delete(Auth.checkToken, Pages.delete);

  //Post Type
  router.route('/posttype')
    .get(Auth.checkToken, PostType.index)
    .post(Auth.checkToken, PostType.post);

  router.route('/posttype/:id')
    .get(Auth.checkToken, PostType.get)
    .put(Auth.checkToken, PostType.put)
    .delete(Auth.checkToken, PostType.delete);

  router.route('/posttype/:id/posts')
    .get(Auth.checkToken, PostType.getPosts)

  //Posts
  router.route('/post')
    .get(Auth.checkToken, Posts.index)
    .post(Auth.checkToken, Posts.post);

  router.route('/post/:id')
    .get(Auth.checkToken, Posts.get)
    .put(Auth.checkToken, Posts.put)
    .delete(Auth.checkToken, Posts.delete);

  //User
  router.route('/user')
    .get(Auth.checkToken, User.index)
    .post(Auth.checkToken, User.post);

  router.route('/user/:id')
    .get(Auth.checkToken, User.get)
    .put(Auth.checkToken, User.put)
    .delete(Auth.checkToken, User.delete);

  //FORMS
  router.route('/forms/:slug')
    .get(Auth.checkToken, Forms.index)

  router.route('/forms/:slug/csv')
    .get(Auth.checkToken, Forms.getCSV)


  //Taxonomy
  router.route('/taxonomy/:target/:type')
    .get(Auth.checkToken, Taxonomy.index)
    .post(Auth.checkToken, Taxonomy.post)

  router.route('/taxonomy/item/:id')
    .get(Auth.checkToken, Taxonomy.get)
    .put(Auth.checkToken, Taxonomy.put)
    .delete(Auth.checkToken, Taxonomy.delete);

  //Relationship
  router.route('/relationship/:object_id')
    .get(Auth.checkToken, Relationship.index)
    .post(Auth.checkToken, Relationship.post)

  router.route('/relationship/:id')
    .delete(Auth.checkToken, Relationship.delete);


  //CUSTOM
  router.route('/custom/:slug')
    .get(Auth.checkToken, Custom.index)
    .post(Auth.checkToken, Custom.post)

  router.route('/custom/item/:id')
    .get(Auth.checkToken, Custom.get)
    .put(Auth.checkToken, Custom.put)
    .delete(Auth.checkToken, Custom.delete);


  //MEDIA
  router.route('/media')
    .get(Auth.checkToken, Media.index)
    .post(Auth.checkToken, Media.post);

  router.route('/media/:id')
    .get(Auth.checkToken, Media.get);

  router.route('/media/:filename')
    .delete(Auth.checkToken, Media.delete);


  return router;
};
