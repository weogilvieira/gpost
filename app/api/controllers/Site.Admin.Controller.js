const PagesModel = require("../../core/models/PagesModel");
const SiteModel = require("../../core/models/SiteModel");
const KeyModel = require("../../core/models/KeyModel");

const fs = require("fs");
 
exports.index = async (req, res, next) => {
  var data = data || {};
  var siteModel = new SiteModel();

  data.data = await JSON.parse(fs.readFileSync("./config.json", "utf8"));
  data.data.keys = await siteModel.load();

  res.status(200).send(data); 
}

exports.keys = async (data, req, res, next) => {
  var data = data;
  var siteModel = new SiteModel();

  data.data = await siteModel.load();

  res.status(200).send(data); 
}


exports.get = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var key = new KeyModel();
  
  await key.get(id)

  data.data = key.data;

  res.status(200).send(data); 
}


exports.put = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;

  var obj = req.body;
  obj['id'] = id;

  var key = new KeyModel(obj);
  data.data = await key.update(obj);
  res.status(200).send(data);
}

exports.post = async (data, req, res, next) => {
  var data = data;
  var obj = req.body;
  var key = new KeyModel(obj);

  data.data = await key.save();

  res.status(200).send(data);
}

exports.delete = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var key = new KeyModel({ id : id });

  await key.delete(id);

  data.data = key.data;
  
  return res.status(200).send(data);
}
