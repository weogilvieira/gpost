const { UserSchema } = require('../../core/schemas');
const UserModel = require('../../core/models/UserModel');

exports.index = (data, req, res, next) => {
  var data = data || {};
  
  if( data.user ){
    data['data'] = data.user;
  }

  return res.status(200).send(data);
}

exports.update = async (data, req, res, next) => {
  var data = data;

  var obj = {
    name: req.body.name,
    email: req.body.email,
    front_role: req.body.front_role,
    bio: req.body.bio
  };


  if(!obj.name || !obj.email ){
    return res.status(200).send({
      message: "Alguns ou todos os campos são requeridos."
    });
  }

  var user = new UserModel(data.user.id);
  var data = await user.update(obj);
  data.data.id = id;

  return res.status(200).send(data);

}


exports.updatePass = async (data, req, res, next) => {
  var data = data;

  if (!req.body.password) {
    data.data = null;
    data.message = "Senha é requerida";
    return res.status(200).send(data);
  }


  var obj = {
    password: req.body.password
  }

  var user = new UserModel();
  await user.get(data.user.id);

  try { 
    await user.update(obj); 
    data.data = user.getUser();
    data.message = "Senha atualizada com sucesso."
  } catch(e) {
    data.data = null;
    data.message = e;
  }

  return res.status(200).send(data);
}
