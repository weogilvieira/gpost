const PagesModel = require("../../core/models/PagesModel");
const PageModel = require("../../core/models/PageModel");
const fs = require('fs');

exports.index = async (data, req, res, next) => {
  var data = data;
  var pages = new PagesModel();

  data.data = await pages.getAll(1);

  res.status(200).send(data); 
}

exports.get = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var page = new PageModel();
  
  await page.get(id);

  data.data = page.data;

  res.status(200).send(data);

}

exports.put = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;

  var obj = req.body;
  obj['id'] = id;
  var page = new PageModel(obj);

  try {
    data.data = await page.update(obj);
    res.status(200).send(data);
  } catch (e) {
    data.message = e;
    res.status(200).send(data);
  }

}

exports.post = async (data, req, res, next) => {
  var data = data;
  var obj = req.body;
  var page = new PageModel(obj);


  try {
    data.data = await page.save();
    res.status(200).send(data);
  } catch (e) {
    data.message = e;
    res.status(200).send(data);
  }

}


exports.delete = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var page = new PageModel({ id : id });
  var childs = await page.getChilds();

  data.data = page.data;

  if( childs.length ) {
    data.data = null;
    data.message = "É necessário desassociar subpáginas.";
    return res.status(200).send(data);
  }
  
  try {
    await page.delete(id);
    res.status(200).send(data);
  } catch (e) {
    data.message = e;
    res.status(200).send(data);
  }

}

exports.getTemplates = async (data, req, res, next) => {
  var data = data;
  data.data = [];

  fs.readdir('./app/front/views/pages/templates', (err, items) => {
    
    if(err) return res.status(200).send(data);

    data.data = items.map(a => {
      return a.replace(".twig", "");
    });

    res.status(200).send(data);
  })
}

exports.getHooks = async (data, req, res, next) => {
  var data = data;
  data.data = [];

  fs.readdir('./app/front/hooks/', (err, items) => {
    if(err) return res.status(200).send(data);

    data.data = items;
    res.status(200).send(data);
  })
}
