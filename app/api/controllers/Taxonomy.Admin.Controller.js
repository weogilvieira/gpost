const TaxonomyModel = require("../../core/models/TaxonomyModel");

exports.index = async (data, req, res, next) => {
  var data = data;
  var items = new TaxonomyModel();
  var page = req.query.page || 1;
  var query = req.query.query || null;
  var type = req.params.type;
  var target = req.params.target;

  data.data = await items.getAll(target, type, page, query).catch(error => {
    data.message = error;
    return res.status(500).send(data);
  });

  return res.status(200).send(data);
}

exports.get = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var taxonomy = new TaxonomyModel();

  await taxonomy.get(id);

  data.data = taxonomy.data;
  
  res.status(200).send(data);

}

exports.put = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var reqBody = req.body;
  var obj = {};

  obj['id'] = id;
  obj['title'] = reqBody.title;
  obj['target'] = reqBody.target;
  obj['type'] = reqBody.type;

  if(!obj.title){
    data.data = null;
    data.message = "Nome é requerido.";
    return res.status(200).send(data);
  }

  var taxonomy = new TaxonomyModel(obj);

  try {
    data.data = await taxonomy.update(obj);
    data.data.id = id;
    return res.status(200).send(data);
  } catch (e) {
    data.message = e;
    return res.status(200).send(data);
  }

}

exports.post = async (data, req, res, next) => {
  var data = data;
  var obj = req.body;

  obj['type'] = req.params.type;
  obj['target'] = req.params.target;
  

  var taxonomy = new TaxonomyModel(obj);

  if(!obj.title){
    data.data = null;
    data.message = "Nome é requerido.";
    return res.status(200).send(data);
  }

  try {
    data.data = await taxonomy.save(obj);
    return res.status(200).send(data);
  } catch (e) {
    data.message = e;
    return res.status(200).send(data);
  }

}


exports.delete = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var item = new TaxonomyModel({ id : id });

  try {
    await item.delete(id);
    data.data = item.data;
    return res.status(200).send(data);
  } catch (e) {
    data.message = e;
    return res.status(200).send(data);
  }
}

