const CustomModel = require("../../core/models/CustomModel");
const csv = require('csv-express')

exports.index = async (data, req, res, next) => {
  var data = data;
  var custom = new CustomModel();
  var page = req.query.page || 1;
  var slug = req.params.slug || null;
  var query = req.query.query || null;

  data.data = await custom.getAll(slug, page, query, 18);

  return res.status(200).send(data);
}


exports.get = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var custom = new CustomModel();

  await custom.get(id);
  data.data = custom.data;
  res.status(200).send(data);
}

exports.put = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;

  var obj = req.body;
  obj['id'] = id;

  var custom = new CustomModel(obj);
  
  data.data = await custom.update(obj);
  data.data.id = id;

  return res.status(200).send(data);
}

exports.post = async (data, req, res, next) => {
  var data = data;
  var obj = req.body;
  var custom = new CustomModel(obj);

  data.data = await custom.save();

  return res.status(200).send(data);
}


exports.delete = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var custom = new CustomModel({ id : id });

  await custom.delete();
  data.data = custom.data;
  
  return res.status(200).send(data);
}

