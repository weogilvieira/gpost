const UserModel = require("../../core/models/UserModel");

exports.index = async (data, req, res, next) => {
  var data = data;
  var User = new UserModel();
  var page = req.query.page || 1;
  var query = req.query.query || null;

  data.data = await User.getAll(page, query, 18);

  return res.status(200).send(data);
}

exports.get = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var user = new UserModel();

  await user.get(id);
  data.data = user.getUser();
  res.status(200).send(data);
}

exports.put = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;

  var obj = req.body;
  obj['id'] = id;

  var user = new UserModel(obj);
  
  data.data = await user.update(obj);
  data.data.id = id;

  return res.status(200).send(data);
}

exports.post = async (data, req, res, next) => {
  var data = data;
  var obj = req.body;
  var user = new UserModel(obj);

  data.data = await user.save();

  return res.status(200).send(data);
}


exports.delete = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var user = new UserModel({ id : id });

  await user.delete();
  data.data = user.data;
  
  return res.status(200).send(data);
}

