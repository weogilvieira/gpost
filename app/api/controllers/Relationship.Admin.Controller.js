const TaxonomyModel = require("../../core/models/TaxonomyModel");
const TaxonomyRelationshipModel = require("../../core/models/TaxonomyRelationshipModel");

exports.index = async (data, req, res, next) => {
  var data = data;
  var object_id = req.params.object_id;

  var taxonomyModel = new TaxonomyRelationshipModel();

  data.data = await taxonomyModel.getAll(object_id).catch(error => {
    data.message = error;
    return res.status(500).send(data);
  });

  return res.status(200).send(data);
}

exports.get = async (data, req, res, next) => {
  var data = data;
  var id = req.params.object_id;
  var taxonomy = new TaxonomyModel();

  await taxonomy.get(id);

  data.data = taxonomy.data;
  
  res.status(200).send(data);

}

exports.post = async (data, req, res, next) => {
  var data = data;
  var obj = req.body;
  var object_id = req.params.object_id;
  var taxonomy_id = obj.taxonomy_id.split(',');

  let items = [];

  taxonomy_id.map( a => {
    items.push( new TaxonomyRelationshipModel({
      object_id: object_id,
      taxonomy_id: a
    }));
  });

  if(!object_id){
    data.data = null;
    data.message = "object_id é requerido.";
    return res.status(200).send(data);
  }

  data.data = [];
  data.message = "";

  try {
    await Promise.all(items.map( async (a) => {
      data.data.push( await a.save() );
    }));
  } catch (e) {
    data.message = e;
  }

  return res.status(200).send(data);
}


exports.delete = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var item = new TaxonomyRelationshipModel({ id : id });

  try {
    await item.delete(id);
    data.data = item.data;
    res.status(200).send(data);
  } catch (e) {
    data.message = e;
    res.status(200).send(data);
  }
}

