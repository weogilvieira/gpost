const PostModel = require("../../core/models/PostModel");
const TaxonomyRelationshipModel = require("../../core/models/TaxonomyRelationshipModel");

exports.index = async (data, req, res, next) => {
  var data = data;
  var query = req.query || {};
  var post_type = req.params.id || null;
  var postsModel = new PostModel();

  await postTypeModel.getAll(query)
    .then( posts => {
      data.data = posts;
      return res.status(200).send(data);
    })
    .catch( error => {
      data.message = error;
      return res.status(500).send(data);
    });
}

exports.get = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var post = new PostModel();

  await post.get(id);
  data.data = post.data;
  res.status(200).send(data);

}

exports.put = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var obj = req.body;
  obj['id'] = id;
  var post = new PostModel(obj);
  
  try {
    data.data = await post.update(obj);
    data.data.id = id;
    return res.status(200).send(data);
  } catch (e) {
    data.message = e;
    return res.status(200).send(data);
  }

}

exports.post = async (data, req, res, next) => {
  var data = data;
  var obj = req.body;
  
  obj.author = data.user.id;

  var post = new PostModel(obj);

  try {
    data.data = await post.save();

    var tags = obj.tags.split(",");
    var taxonomy_promisses = [];

    tags.map( a => {
      taxonomy_promisses.push( new TaxonomyRelationshipModel({
        object_id: data.data.id,
        taxonomy_id: Number(a)
      }));
    });

    await Promise.all(taxonomy_promisses.map( async (b) => {
      await b.save();
    }));

    return res.status(200).send(data);
  } catch (e) {
    data.message = e;
    return res.status(200).send(data);
  }
}


exports.delete = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var post = new PostModel({ id : id });

  try {
    await post.delete(id);
    data.data = post.data;
    return res.status(200).send(data);
  } catch (e) {
    data.message = e;
    return res.status(200).send(data);
  }

}

