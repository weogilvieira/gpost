const AuthModel = require('../../core/models/AuthModel');

exports.index = async (req, res) => {
  var auth = new AuthModel(req.body.email, req.body.password);

  auth.run().then( auth => {
    return res.status(200).send(auth);
  }); 

}


exports.logout = (req, res) => {
  return res.status(200).send("logout");
}


exports.checkToken = async (req, res, next) => {
  var token = req.headers['x-access-token'] || req.body.token || req.query.token;
  var authModel = new AuthModel();

  var auth = await authModel.checkToken(token);

  if(!auth) return res.status(401).send("Invalid token.");
  
  if( req.body.hasOwnProperty('token') ) { delete req.body.token; }
  if( req.query.hasOwnProperty('token') ) { delete req.query.token; }

  return next(auth);
}
