const MediaModel = require('../../core/models/MediaModel');
const multer = require('multer');
const slugify = require('slugify');
const path = require('path');
const fs = require('fs');
const sharp = require('sharp');
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');

const upload_storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, '../../public/media/tmp'))
  },
  filename: function (req, file, cb) {
    var hash = Math.random().toString(36).substring(2, 4) + Math.random().toString(36).substring(2, 3);
    cb(null, `${slugify(file.originalname.split('.').slice(0, -1).join('.'), { lower : true })}${hash}${path.extname(file.originalname)}`)
  }
});

var uploadAdapter = multer({ 
  storage : upload_storage,
  fileFilter: function(req, file, cb) {

    if ( !(/\.(gif|jpg|jpeg|png)$/i).test(path.extname(file.originalname)) ) {
      return cb("Tipo de arquivo inválido.")
    }

    cb(null, true);
  }
}).single('upload');


exports.index = async (data, req, res, next) => {
  var data = data;
  var media = new MediaModel();
  var page = req.query.page || 1;

  var q = null;
  if( req.query && req.query.hasOwnProperty('filename') ){
    q = {};
    q.filename = req.query.filename;
  }

  data.data = await media.getAll(page, q);

  res.status(200).send(data);
}

exports.post = async (data, req, res, next) => {
  var data = data;
      data.uploaded = false;

  var dir = path.join(__dirname, '../../public/media/tmp');
  if (!fs.existsSync(dir)){ fs.mkdirSync(dir); }

  return uploadAdapter(req, res, async function(err){

    if( err ) {
      data['message'] = typeof err == "string" ? err : "Problemas ao enviar o arquivo, tente novamente.";
      data.error = {};
      data.error.message = data.message;
      return res.status(200).send(data);
    }

    var file = req.file;
    if( !file ) return res.status(200).send(data);

    try {
      await imagemin([file.path], path.join(__dirname, '../../public/media/'), {
        plugins: [
          imageminJpegtran({ progressive: true }),
          imageminMozjpeg({
            quality: 70
          }),
          imageminPngquant({quality: [0.6, 0.8]})
        ]
      });
    } catch (e) {
      data.data = null;
      data.message = e;
      return res.status(200).send(data);
    }

    await fs.unlink(`${file.path}`, (err) => {  if (err) console.log(err); });

    var mediaModel = new MediaModel({
      filename: file.filename
    });

    await mediaModel.save().then( async (r) => {
      data.uploaded = true;
      data.data = mediaModel.data;
      data.urls = await mediaModel.get();
      data.message = null;
      return res.status(200).send(data);
    }).catch( r => {
      data.message = r;
      data.error = { message: r }
      return res.status(200).send(data);
    });

  });

}

exports.delete = async (data, req, res, next) => {
  var data = data;
  var filename = req.params.filename;
  var media = new MediaModel();

  data.data = await media.delete(filename);

  res.status(200).send(data);
}


exports.get = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var mediaModel = new MediaModel({ id: id });
  await mediaModel.get();

  data.data = mediaModel.data;

  res.status(200).send(data);
}
