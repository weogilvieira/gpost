const FormModel = require("../../core/models/FormModel");
const csv = require('csv-express')

exports.index = async (data, req, res, next) => {
  var data = data;
  var form = new FormModel();
  var page = req.query.page || 1;
  var slug = req.params.slug || null;

  data.data = await form.getAll(slug, page);

  return res.status(200).send(data);
}


exports.getCSV = async (data, req, res, next) => {
  var data = data;
  var form = new FormModel();
  var page = req.query.page || 1;
  var slug = req.params.slug || null;
  var csvObj = [];

  data.data = await form.getAll(slug, null, false);

  let all_inputs = await Promise.all(data.data.map((a) => {
    let inputs = JSON.parse(a.inputs);
    csvObj.push(inputs);
  }));

  csv.separator = ";";
  return res.csv(csvObj, true);

}



exports.get = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var form = new FormModel();

  await form.get(id);
  data.data = form.getUser();
  res.status(200).send(data);
}

exports.put = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;

  var obj = req.body;
  obj['id'] = id;

  var user = new FormModel(obj);
  
  data.data = await user.update(obj);
  data.data.id = id;

  return res.status(200).send(data);
}

exports.post = async (data, req, res, next) => {
  var data = data;
  var obj = req.body;
  var user = new FormModel(obj);

  data.data = await user.save();

  return res.status(200).send(data);
}


exports.delete = async (data, req, res, next) => {
  var data = data;
  var id = req.params.id;
  var user = new FormModel({ id : id });

  await user.delete();
  data.data = user.data;
  
  return res.status(200).send(data);
}

