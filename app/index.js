const express = require('express'),
      http    = require('http'),
      twig    = require('twig'),
      router  = express.Router();
      path    = require('path'),
      bodyParser = require("body-parser"),
      compression = require('compression'),
      i18n = require("i18n-express"),
      schemas = require("./core/schemas"),
      minifyHTML = require('express-minify-html'),
      cors = require('cors');

//INIT SERVER
const app = express();
app.set('port', process.env.PORT || 3000);

//SET TEMPLATE ENGINE
twig.cache( ('production' == process.env.NODE_ENV) );

app.set("twig options", {
  allow_async: true,
  strict_variables: false,
  namespaces: {
    "amp" : path.join( __dirname + '/template/amp/views'),
    "main" : path.join( __dirname + '/template/main/views'),
  }
});

app.set('views', path.join( __dirname + '/template'));
app.set('view engine', 'twig');

// app.use(minifyHTML({
//     override:      true,
//     exception_url: true,
//     htmlMinifier: {
//       removeComments:            true,
//       collapseWhitespace:        true,
//       collapseBooleanAttributes: true,
//       removeAttributeQuotes:     true,
//       removeEmptyAttributes:     true,
//       minifyJS:                  true,
//     }
// }));

app.use(compression());

//SET LOCAL PARAMS
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.use((req, res, next) => {
  let port = process.env.PORT || 3000;

  if(req.url.endsWith('.jpg') || 
      req.url.endsWith('.jpeg') || 
      req.url.endsWith('.png') ||
      req.url.endsWith('.svg') ||
      req.url.endsWith('.gif')){ 

    res.set({ 'Cache-Control':'public, max-age=31536000' });

  };

  if( process.env.NODE_ENV == "development" ){
    res.locals.BASE_URL = process.env.SITE_BASE_URL;
  } else {
    res.locals.BASE_URL = req.protocol + 's://' + req.hostname  + ((process.env.NODE_ENV == "production" || port == 80 || port == 443) ? '' : ':' + port) + '/';
  }
  
  next();
});


//SET STATIC
app.use(express.static(path.join(__dirname, 'public')));
app.use('/admin', express.static(path.join(__dirname, 'admin/')));

//SET ROUTES
const apiRoutes = require(path.join(__dirname, '/api/routes.js'))(app);
const frontRoutes = require(path.join(__dirname, '/front/routes.js'))(app);

app.use('/api', cors({ credentials: true, origin: (origin, cb) => { return cb(null, true); } }), apiRoutes);
app.use('/', frontRoutes);

app.use("/admin", ( req, res ) => { res.sendFile('admin/index.html', { root: __dirname }); });

// app.get("*", (req,res) => { res.status(404).render('4xx', { title: 'Not Found' }); });
// app.get("*", (err,req,res) => { res.status(500).render('5xx', { title: 'Erro de Servidor' }); });

//LISTEN SERVER
const server = http.createServer(app);
schemas.sequelize
  .authenticate()
  .then(() => {
    server.listen(app.get('port'), '0.0.0.0', () => {
      console.log("Express server listening on port " + app.get('port'));
    });
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });
