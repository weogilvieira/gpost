const PostModel = require("../../core/models/PostModel");

exports.index = async (data, req, res, next) => {
  var data = data || {};
  var posts = new PostModel();
  var highlighted_arr = [];
  var type = req.query.type || "html";
  data.page = req.query.page || 1;

  data.posts = await posts.getAll({
   page: 1,
   paginate: 5
  });


  // if( type == "infinite" ) return res.render("index-infinite.twig", { data: data });

  res.render("main/views/index", { data: data });
}
