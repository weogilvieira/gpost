const PostTypeModel = require("../../core/models/PostTypeModel");
const PostModel = require("../../core/models/PostModel");
const TaxonomyModel = require("../../core/models/TaxonomyModel");
const TaxonomyRelationshipModel = require("../../core/models/TaxonomyRelationshipModel");
const ampify = require('ampify');
const cheerio = require('cheerio');
const { extract } = require('oembed-parser');

exports.index = async (data, req, res, next) => {

  const slug = req.params.page;
  const postTypeModel = new PostTypeModel();
  const page = req.query.page || 1;
  const postType = await postTypeModel.get({ slug: slug });


  if(!postType){ return next(data); }

  data.postType = postType;

  data.posts = await data.postType.getPosts({
    status: 1,
    page: page
  });

  data._SITE.setSeo({
    title: `${data.postType.data.title} página ${page}`,
    description: `Confira ${data.postType.data.title} de ${data._SITE.getConfig('sitename')}. Página ${page}`
  });

  return res.render(`main/views/post/${data.postType.data.slug}-index`, { data: data }, function(err, template) {
    if (err) {
      if (err.message.indexOf('Failed to lookup view') !== -1) {
        return res.render("main/views/post/", { data: data });
      }
      throw err;
    }
    res.send(template);
  });

}

exports.detail = async (data, req, res, next) => {
  
  const postTypeSlug = req.params.page;
  var postSlug = req.params.child;

  if(!postSlug){ return next(data); }

  const isAmp = postSlug.split(".").pop() == 'amp';

  if(isAmp) {
    postSlug = postSlug.split(".");
    postSlug.pop();
    postSlug.join(".").toString();
  }

  const postTypeModel = new PostTypeModel();
  const postType = await postTypeModel.get({ slug: postTypeSlug });

  if(!postType){ return next(data); }

  const post = new PostModel();
  await post.get({ "post_type" : postType.data.id, "slug" : postSlug });
  await post.populate();

  if(!post.data) return next(data);
  
  data.breadcrumbs.push({
    title: `${postType.data.title}`,
    path: `/${postType.data.slug}`
  });

  data.postType = postType;
  data.post = post.data;

  data._SITE.setSeo({
    title: post.getSeo('title'),
    description: post.getSeo('description'),
    cover: (data.post.cover ? data.post.cover.default : "")
  });

  const category = await post.getCategory();

  data.breadcrumbs.push({
    title: `${category.data.title}`,
    path: `/${postType.data.slug}/${category.data.slug}`
  });

  let postModel = new PostModel();
  let related_posts = await postTypeModel.getPosts({
    random: true,
    paginate: 3,
    except: [data.post.id],
    category: category.data.id
  });

  data.related_posts = related_posts;

  const $html = cheerio.load(data.post.content);

  var oembedProms = [];

  $html('figure.media').each(function(a, b){
    $html(b).replaceWith(`<div class="media">${$html(b).html()}</div>`);
  });

  $html('oembed').each(function(a, b){

    let href = ($html(b).attr('url') || "").replace("http://", "https://");
    if( href.indexOf("youtu.be") >= 0 ){
      href = href.split("youtu.be/")[1];
      href = "https://www.youtube.com/watch?v=" + href;
    }

    $html(b).attr("href", href);

    oembedProms.push(new Promise(function(resolve, reject) {
      extract(href).then((oembed) => {
        $html(b).replaceWith( !isAmp ? oembed.html : `<a href="${href}" target="_blank" rel="nofollow">Visualizar em ${oembed.provider_name}</a>`);
        resolve(true);
      }).catch((err) => {
        $html(b).replaceWith(`<a href="${href}" target="_blank">${href}</a>`);
        reject();
      });
    }));

  });

  await Promise.all(oembedProms);

  data.post.content = $html.html();


  if( isAmp ) {

    var ampHTML = await ampify(data.post.content, { 'cwd' : '', 'round' : false });
    const $amp = cheerio.load(ampHTML);

    $amp('amp-img').addClass('contain').attr("layout", "responsive");

    data.post.content = await $amp('body').html();

    return res.render('amp/views/post/'+postType.data.slug+'-detail', { data: data }, function(err, template) {
      if (err) {
        if (err.message.indexOf('Failed to lookup view') !== -1) {
          return res.render("amp/views/post/detail", { data: data });
        }
        throw err;
      }
    });
  }

  return res.render('main/views/post/'+postType.data.slug+'-detail', { data: data }, function(err, template) {
    if (err) {
      if (err.message.indexOf('Failed to lookup view') !== -1) {
        return res.render("main/views/post/detail", { data: data });
      }
      throw err;
    }
    res.send(template);
  });
}

exports.tag = async (data, req, res, next) => {
  const taxonomyModel = new TaxonomyModel();
  const tag = await taxonomyModel.get({ slug: req.params.slug });

  if(!tag) return next(data);
  data.tag = tag;
  data.posts = [];
  data.PAGE = req.query.page || 1;

  data.breadcrumbs.push({
    title: `${data.tag.data.title}`,
    path: `${data.tag.getLink()}`
  });

  data._SITE.setSeo({
    title: `Tag: ${data.tag.data.title} - Página ${data.PAGE}`,
    description: `Veja todos os posts da tag "${data.tag.data.title}", página ${data.PAGE}.`
  });

  const relationsshipModel = new TaxonomyRelationshipModel();
  const relationsships = await relationsshipModel.getAll(null, data.tag.data.id);
  

  relationsships.map(a => {
    if( data.posts.indexOf(a.object_id)==-1){
      data.posts.push(a.object_id);
    }
  });

  const postModel = new PostModel();
  data.posts = await postModel.getAll(data.posts, data.PAGE);

  return res.render("main/views/post/tag", { data: data });
}

exports.category = async (data, req, res, next) => {
  const postTypeSlug = req.params.page;
  const categorySlug = req.params.child;

  if(!categorySlug){ return next(data); }

  const postTypeModel = new PostTypeModel();
  const page = req.query.page || 1;
  const postType = await postTypeModel.get({ slug: postTypeSlug });

  if(!postType){ return next(data); }

  const taxonomyModel = new TaxonomyModel();
  const category = await taxonomyModel.get({ slug: categorySlug, type: 'category', target: postType.data.id });

  console.log(category);

  if( !category ) { return next(data); }


  data.postType = postType;
  data.category = category;

  data.breadcrumbs.push({
    title: `${data.postType.data.title}`,
    path: `/${data.postType.data.slug}`
  });

  const posts = await postTypeModel.getPosts({
    status: 1,
    page: page,
    category: data.category.data.id
  });

  data.posts = posts;

  data._SITE.setSeo({
    title: data.category.data.title,
    description: `Veja todos os posts de ${data.category.data.title}.`
  });


  return res.render('main/views/post/'+data.postType.data.slug+'-category', { data: data }, function(err, template) {
    if (err) {
      if (err.message.indexOf('Failed to lookup view') !== -1) {
        return res.render("main/views/post/category", { data: data });
      }
      throw err;
    }
    res.send(template);
  });
}
