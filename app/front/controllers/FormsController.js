const fs = require("fs");
const FormModel = require("../../core/models/FormModel");
const SiteConfig = JSON.parse(
  fs.readFileSync("./config.json", "utf8")
);

exports.index = async (data, req, res, next) => {
  var data = data || {};
  var slug = req.params.slug;
  var obj = req.body || {};

  var inputs = {};
  var errors = [];

  var formConfig = await SiteConfig.forms.filter( a => {
    return (a.key == slug);
  });

  if(!formConfig.length){  return next(); }

  await Object.keys(formConfig[0].inputs).map(a => {
    inputs[a] = obj.hasOwnProperty(a) ? (obj[a] || "") : "";

    if( formConfig[0].inputs[a].required && !inputs[a].length ){
      errors.push(`"${formConfig[0].inputs[a].title}" is required.`);
      return;
    }

    if( formConfig[0].inputs[a].minLength && formConfig[0].inputs[a].minLength < inputs[a].length ){
      errors.push(`"${formConfig[0].inputs[a].title}" min length is ${formConfig[0].inputs[a].minLength}.`);
      return;
    }

    if( formConfig[0].inputs[a].maxLength && formConfig[0].inputs[a].maxLength < inputs[a].length ){
      errors.push(`"${formConfig[0].inputs[a].title}" max length is ${formConfig[0].inputs[a].maxLength}.`);
      return;
    }

  });

  if( errors.length ) return res.status(200).send({
    data: inputs,
    message: errors[0],
    errors: errors,
    formConfig: formConfig.inputs
  });

  var form = new FormModel(slug, inputs);

  await form.save();

  return res.status(200).send({
    data: form.data,
    message: `Formulário de "${formConfig[0].title}" enviado com sucesso.`,
    errors: null,
    form: form
  });
}
