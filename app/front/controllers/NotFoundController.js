exports.index = (data, req, res, next) => {
  var data = data || {};

  data._SITE.setSeo({
    title: "Não encontrado",
    description: "Página não encontrada"
  });

  return res.status(404).render("main/views/4xx", { data: data });
}
