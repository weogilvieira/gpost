const sharp = require('sharp');
const fs = require('fs');
const sizeOf = require('image-size');
const path = require('path');
const SiteConfig = JSON.parse(
  fs.readFileSync("./config.json", "utf8")
);

exports.getThumb = (data, req, res, next) => {

  var filename = req.params.filename;
  var size = req.params.size;
  var path_media = "../../public/media/";

  var size_list = SiteConfig.images_sizes;

  if(!size_list.hasOwnProperty(size)){
    return res.sendStatus(404);
  };

  var originalFile = path.join(__dirname, `${path_media}${filename}`);
  var ext = path.extname(filename);

  if( !fs.existsSync(originalFile) ){ 
    res.sendStatus(404); 
    return;
  }

  var dimensions = sizeOf(originalFile);

  if( size_list[size].width >= dimensions.width || ext == ".gif"){ 
    return res.sendFile(originalFile); 
  }

  var pathImg = path.join(__dirname, `${path_media}${size}/${filename}`);
  
  if( fs.existsSync(pathImg) ){ 
    return res.sendFile(pathImg); 
  }

  var transform = sharp(originalFile);
      transform = transform
                .toFormat(ext.replace(".", ""))
                .resize(size_list[size].width, size_list[size].heigth);



    if(!size_list[size].crop){ transform = transform.resize({ fit: "inside" }); }


  var dir = path.join(__dirname, `${path_media}${size}`);
  if (!fs.existsSync(dir)){ fs.mkdirSync(dir); }

  transform.toFile(pathImg, function(err) {
    if(err) return res.sendStatus(500);
    return res.sendFile(pathImg);
  });

};
