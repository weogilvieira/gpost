const PagesModel = require("../../core/models/PagesModel");
const PageModel = require("../../core/models/PageModel");
const fs = require("fs");
const path = require("path");

var obj = {}; 

obj.index = async (data, req, res, next) => {
  var page_slug = req.params.page || null;
  var child_slug = req.params.child || null;
  var page = new PageModel();
  var child = new PageModel();

  await page.get({ slug: page_slug, parent: "" });

  if(!page.data){ 
    return next(data); 
  }


  if(child_slug){
    await child.get({ slug: child_slug, parent: page.data.id });
    if(!child.data){ return next(data); }
  }

  data.page = child_slug ? child : page;

  if( data.page.data ){
    
    data._SITE.setSeo({
      title: data.page.getSeo('title'),
      description: data.page.getSeo('description'),
    });

    data.breadcrumbs.push({
      title: `${page.data.title}`,
      path: `/${page.data.slug}`
    });

    if(data.page.data.page_hook) {

      let hookfile = path.resolve(`./front/hooks/${data.page.data.page_hook}`);
      
      return await fs.exists(hookfile, async function(exists) {

        if(!exists) return res.render(data.page.getTemplate(), { data: data });
        let hook = await require(hookfile);
        await hook(data, req, res, function(data){
          return res.render(data.page.getTemplate(), { data: data });
        });

      });

    }

    return res.render(data.page.getTemplate(), { data: data });

  } else {
    return next(data);
  }
}


module.exports = obj;
