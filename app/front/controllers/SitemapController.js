const fs = require("fs");

const SiteModel = require('../../core/models/SiteModel');
const PagesModel = require("../../core/models/PagesModel");
const { buildSitemaps } = require('express-sitemap-xml');
const SiteConfig = JSON.parse(
  fs.readFileSync("./config.json", "utf8")
);

exports.index = async (data, req, res, next) => {
  var pagesModel = new PagesModel();
  var postsModel = new PostsModel();

  var pages = await pagesModel.getAll();

  const urls = ['/'];

  var parents = pages.filter( (a) => {
    if(!a.parent){
      urls.push(a.slug);
      
      pages.map( b => {
        if(b.parent == a.id){
          urls.push(`${a.slug}/${b.slug}`);
        }
      });
      return true;
    }
  });


  let all_posts = await Promise.all(SiteConfig.post_types.map( (a) => {
     urls.push(a.prefix);
     return postsModel.getPosts(a.prefix, 1, null, 999);
  }));

  all_posts.forEach(function(posts) {
     posts.docs.map( (b) => {
     var l = b.getLink(); 
       urls.push(l);
     });  
  });

  const sitemaps = await buildSitemaps(urls, data._SITE_URL);
  res.type('application/xml').send(sitemaps['/sitemap.xml']);
}
