const PostModel = require("../../core/models/PostModel");
const TaxonomyModel = require("../../core/models/TaxonomyModel");

exports.index = async (data, req, res, next) => {
  var terms = req.query.s;
  var PAGE = req.query.page || 1;
  var data = data;

  var postModel = new PostModel();
  var taxonomyModel = new TaxonomyModel();

  var posts = await postModel.getAll({
    terms: terms,
    page: PAGE
  });
  var categories = await taxonomyModel.getAll(null, 'category', 1, terms);
  var tags = await taxonomyModel.getAll(null, 'tag', 1, terms);

  data = {
    ...data,
    terms: terms,
    PAGE: PAGE,
    posts: posts,
    categories: categories,
    tags: tags
  };

  return res.render("main/views/search.twig", { data: data });
}
