const fs = require("fs");

const SiteModel = require('../../core/models/SiteModel');
const TaxonomyModel = require('../../core/models/TaxonomyModel');
const SiteConfig = JSON.parse(
  fs.readFileSync("./config.json", "utf8")
);

exports.index = async (req, res, next) => {
  var siteModel = new SiteModel();
  var taxonomyModel = new TaxonomyModel();
  var categories = await taxonomyModel.getAll("posts", "category", 1, null);

  await siteModel.load();

  siteModel.load().then(function(){

    let _SITE_URL = ( process.env.SITE_BASE_URL || req.protocol + '://' + req.get('host'));

    let _CANONICAL_URL = _SITE_URL + req.originalUrl;

    var data = {
      _SITE: siteModel,
      _SITE_URL: _SITE_URL,
      _CANONICAL_URL: _CANONICAL_URL,
      _CATEGORIES: categories,
      _SITE_CONFIG: SiteConfig,
      breadcrumbs: [{
        title: "Home",
        path: "/"
      }]
    };
    
    return next(data);

  }).catch( err => {
    console.log("Error base controller => ", err);
  });

}
