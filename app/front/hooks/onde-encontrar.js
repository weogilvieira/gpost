const CustomModel = require("../../core/models/CustomModel");

module.exports = async (data, req, res, next) => {
  var custom = new CustomModel();
  data.stores = await custom.getAll('stores', 1, null, false);

  data.stores = data.stores.map(a => {
    return new CustomModel(a);
  });
    
  return next(data);
};
