module.exports = async (data, req, res, next) => {
  data.example_hook = "this is a example hook";
  return next(data);
};
