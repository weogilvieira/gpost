const express = require('express');
const router = express.Router();
const fs = require("fs");
const mcache = require('memory-cache');
 
const SiteConfig = JSON.parse(
  fs.readFileSync("./config.json", "utf8")
);

// const Sitemap = require('./controllers/SitemapController');
const Base = require('./controllers/BaseController');
const Home = require('./controllers/HomeController');
const Post = require('./controllers/PostController');
const Page = require('./controllers/PageController');
const Search = require('./controllers/SearchController');
const NotFound = require('./controllers/NotFoundController');
const Media = require('./controllers/MediaController');
const Forms = require('./controllers/FormsController');

var cache = (duration) => {
  return (req, res, next) => {
    let key = '__express__' + req.originalUrl || req.url
    let cachedBody = mcache.get(key)
    if (cachedBody) {
      res.send(cachedBody)
      return
    } else {
      res.sendResponse = res.send
      res.send = (body) => {
        mcache.put(key, body, duration * 1000);
        res.sendResponse(body)
      }
      next()
    }
  }
}


module.exports = (app) => {

  router.get('/media/:size/:filename', Base.index, Media.getThumb);

  // HOME
  router.get('/', Base.index, Home.index);
  router.get('/buscar', Base.index, Search.index);
  router.get('/404', Base.index, NotFound.index);

  //FORMS
  router.post('/forms/:slug', Base.index, Forms.index);

  // Dynamic content
  router.get('/tag/:slug', Base.index, Post.tag, NotFound.index);
  router.get('/:page', Base.index, Page.index, Post.index, NotFound.index);
  router.get('/:page/:child', Base.index, Page.index, Post.category, Post.detail, NotFound.index);

  router.get("*", Base.index, NotFound.index);

  return router;

};
