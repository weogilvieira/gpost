const { PageSchema } = require('../../core/schemas');

function Model(data) {
  this.data = data || {};
}

Model.prototype.getAll = async function() {
  return await PageSchema.findAll({
    attributes: ['id', 'title', 'slug', 'parent']
  });
};

Model.prototype.getChilds = async function(id) {
  if(!id){ return; }
  
  var pages =  await PageSchema.findAll({
    where: { parent: id },
    attributes: ['id', 'title', 'slug', 'parent']
  });

  return pages;

}

Model.prototype.getPage = async function(page_slug, child_slug = null) {
  return PageSchema.findOne({ where: { slug: page_slug }}).then(page => {
    if(!page){ return false; }
    this.data = page;
    return this;
  });
};

module.exports = Model;
