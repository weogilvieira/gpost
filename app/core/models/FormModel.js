var { FormSchema } = require('../../core/schemas');

function Model(key, inputs) {
  this.data = {};
  this.data.inputs = JSON.stringify(inputs || {});
  this.data.key = key || "";
}

Model.prototype.getAll = async function(key = null, page = 1, paginate = 20) {

  if(!paginate) return await FormSchema.findAll({ where: { key: key }, order: [[ 'id', 'DESC' ]] });

  if(page < 1){ page = 1; }
  var obj = {
    page: page,
    paginate: paginate,
    order: [['id', 'DESC']],
    where: {
      key: key
    }
  };

  return await FormSchema.paginate(obj);
};

Model.prototype.get = async function(param) {
  if(!param){ return; }

  let obj = {};

  if( typeof param == "string" && isNaN(param) ){ obj = { key: param }; };
  if( !isNaN(param) ){ obj = { id: param }; };
  if( typeof param == "object" ){ obj = param; };

  return await FormSchema.findOne({ where: obj }).then( res => {
    this.data = res;
    // return this.data;
  });
};


Model.prototype.save = async function(obj) {
  
  var obj = obj || this.data;

  if( obj.inputs && typeof obj.inputs == 'object'){ obj.inputs = JSON.stringify(obj.inputs); }
  return await FormSchema.create(this.data).then( r => {
    this.data = r;
    return this.data;
  });
}


Model.prototype.delete = async function() {
  return await FormSchema.destroy({ where: { id: this.data.id }}).then(r => {
    return this.data;
  });
}

module.exports = Model;
