const slugify = require('slugify');
const { TaxonomySchema, TaxonomyRelationshipSchema, PostTypeSchema, sequelize } = require('../../core/schemas');
const TaxonomyRelationshipModel = require("../../core/models/TaxonomyRelationshipModel");

function Model(data) {
  this.data = data || {};
}

Model.prototype.getAll = async function(target = null, type = 'category', page = 1, query = null) {

  let obj = {
    attributes: ['id', 'title', 'slug', 'target', 'type'],
    order: [['id', 'DESC']],
    where: {}
  };


  if( target && type != "tag" ) obj.where['target'] = target;
  if( type ) obj.where['type'] = type;

  if(query){
    obj.where['title'] = { [sequelize.Op.like]: `%${query}%` }
  }

  var res = await TaxonomySchema.findAll(obj);
  
  if( res.length ){
    res = await res.map( async (a) => {
      a = new Model(a);
      await a.populate();
    });
    return res;
  } else {
    return res;
  }

};

Model.prototype.getRange = async function(ids, type) {
  
  let obj = {
    where: {
      type: type
    }
  };

  obj.where['id'] = { [sequelize.Op.in] : ids };

  var res = await TaxonomySchema.findAll(obj);
  
  if( res && res.docs ){
    res.docs = await res.docs.map(a => {
      return new Model(a);
    });

    return res;
  } else {
    return res;
  }

}; 


Model.prototype.get = async function(param) {
  if(!param){ return; }

  let obj = {};

  if( typeof param == "string" && isNaN(param) ){ obj = { slug: param }; };
  if( !isNaN(param) ){ obj = { id: param }; };
  if( typeof param == "object" ){ obj = param; };

  return await TaxonomySchema.findOne({ where: obj }).then( res => {
    this.data = res;
    return res ? new Model(this.data).populate() : null;
  });
};

Model.prototype.getLink = async function(base = null){
  return await PostTypeSchema.findOne({ where: { id : this.data.target} }).then( postType => {
    this.data.link = (this.data.type != "tag") ? `${base || ""}/${postType.slug}/${this.data.slug}/` : `${base || ""}/tag/${this.data.slug}`;
    return this.data.link;
  });
};


Model.prototype.update = async function(obj) {

  if( obj ){ this.data = obj; }

  this.data.slug = slugify(this.data.title.toLowerCase());
  this.data.slug_key = `${this.data.target}/${this.data.type}/${this.data.slug}`;

  return await TaxonomySchema.update({ title: this.data.title, slug: this.data.slug, slug_key: this.data.slug_key }, { where: { id: this.data.id }}).then(r => {
    this.data = r;
    return this.data;
  });
}

Model.prototype.save = async function(obj) {
  
  if( obj ){ this.data = obj; }

  if(!this.data || !this.data.title){ return; }

  this.data.slug = slugify(this.data.title.toLowerCase());
  var _self = this;

  this.data.slug_key = `${this.data.target}/${this.data.type}/${this.data.slug}`;

  return await TaxonomySchema.create(this.data).then( r => {
    _self.data = r;
    return _self.data;
  });
}

Model.prototype.populate = async function() {
  await this.getLink();
};


Model.prototype.delete = async function() {

  return await TaxonomySchema.destroy({ where: { id: this.data.id }}).then(r => {
    return this.data;
  });

  await TaxonomyRelationshipSchema.destroy({ where: { taxonomy_id: this.data.id }}).then(r => {});

}

module.exports = Model;
