const { PostSchema, TaxonomyRelationshipSchema, PostTypeSchema, sequelize } = require('../../core/schemas');
const MediaModel = require("../../core/models/MediaModel");
const TaxonomyModel = require("../../core/models/TaxonomyModel");
const UserModel = require("../../core/models/UserModel");
const TaxonomyRelationshipModel = require("../../core/models/TaxonomyRelationshipModel");
const moment = require('moment');
moment.locale('pt-br');

function Model(data) {
  this.data = data || {};
}

Model.prototype.get = async function(param) {
  if(!param){ return; }

  let obj = {};

  if( typeof param == "string" && isNaN(param) ){ obj = { slug: param }; };
  if( !isNaN(param) ){ obj = { id: param }; };
  if( typeof param == "object" ){ obj = param; };

  return await PostSchema.findOne({ where: obj }).then( post => {
    this.data = post;
    return this.data;
  });
};

Model.prototype.getAll = async function(query = {}) {

  var obj = {
    attributes: ['id', 'title', 'slug', 'createdAt', 'updatedAt', 'published_date', 'status', 'description', 'cover', 'category', 'post_type', 'content', 'highlighted'],
    page: 1,
    paginate: 10,
    order: [],
    where: {}
  };

  if( query.order ) {
    obj.order = [];
    Object.keys(query.order).map( a => {
      obj.order.push([a, query.order[a]])
    });
  } else {
    obj.order.push(['published_date', 'DESC']);
  }

  if( query.published_date ) {
    obj.where.published_date = new Date(query.published_date);
    delete query.published_date;
  }

  if( query.page ){
    obj.page = query.page;
    delete query.page;
  }

  if( query.paginate ){
    obj.paginate = query.paginate;
    delete query.paginate;
  }

  if( query && (typeof query == 'string' || query.hasOwnProperty('terms')) ) {
    let q = (typeof query == 'string' ? query : query.terms ) || '';
    obj.where[sequelize.Op.or] = {
      title: {
        [sequelize.Op.like]: `%${q}%`
      },
      content: {
        [sequelize.Op.like]: `%${q}%`
      },
      description: {
        [sequelize.Op.like]: `%${q}%`
      },
      seo_description: {
        [sequelize.Op.like]: `%${q}%`
      },
      seo_title: {
        [sequelize.Op.like]: `%${q}%`
      }
    }

    delete query.terms;
  };

  if(query.except && typeof query.except == 'object' && query.except.length) { 
    obj.where['id'] = { [sequelize.Op.notIn] : query.except }; 
    delete query.except;
  }

  if(query.random) { 
    obj['order'] = [sequelize.literal('RAND()')]; 
    delete query.random;
  }

  if( query && typeof query == 'object' && !Array.isArray(query) ) {
    Object.keys(query).map(a => {
      obj.where[a] = query[a];
    });
  }

  if( Array.isArray(query) ) {
    obj.where['id'] = query;
  }

  var res = await PostSchema.paginate(obj);
  if( res && res.docs ){
    
    res.docs = await Promise.all(res.docs.map(async (a) => {
      let item = new Model(a);
      await item.populate();
      return item;
    }));

    return res;
  } else {
    return res;
  }
};

Model.prototype.getSeo = function(slug) {
  let r = this.data['seo_' + slug] || this.data[slug] || null;
  return r;
};

Model.prototype.populate = async function(){
  if(!this.data){ return; }
  this.data.link = await this.getLink();
  this.data.description = await this.getDescription();
  this.data.cover = await this.getCover();
  this.data.category = await this.getCategory();
  this.data.tags = await this.getTags();
  this.data.author = await this.getAuthor();
  return this.data;
}


Model.prototype.getCover = async function(size = false){
  if(!this.data.cover){ return ""; }
  let media = new MediaModel({ id: this.data.cover });
  var mediaObj = await media.get();

  if(!mediaObj){ return; }
  this.data.cover = mediaObj;
  
  return size ? `${mediaObj[size]}` : mediaObj;
}

Model.prototype.getDate = async function(format = null){
  return format ? moment(this.data.published_date).format(format) : moment(this.data.published_date).fromNow();
}

Model.prototype.getDescription = async function(){
  return this.data.description ? this.data.description : this.data.content;
};

Model.prototype.getLink = async function(base = ""){
  return await PostTypeSchema.findOne({ where: { id : this.data.post_type} }).then( postType => {
    return `${base}/${postType ? postType.slug : this.data.post_type }/${this.data.slug}`;
  });
};

Model.prototype.getCategory = async function(){
  let cat = new TaxonomyModel();
  await cat.get(this.data.category);
  return cat;
}

Model.prototype.getAuthor = async function(){
  let user = new UserModel();
  await user.get(this.data.author);
  return user.getUser();
}


Model.prototype.getLinkAmp = function(base = ""){
  return `${this.getLink()}?amp=1`;
};


Model.prototype.getTags = async function(){
  var taxsModel = new TaxonomyRelationshipModel();
  let taxs = await taxsModel.getAll(this.data.id);
  let tagsIds = [];
  taxs.map(a => tagsIds.push(a.taxonomy_id) );
  let tagsModel = new TaxonomyModel();
  let tags = await tagsModel.getRange(tagsIds, 'tag');
  
  tags = await Promise.all(tags.map(async (a) => {
    return new TaxonomyModel(a);
  }));

  return tags;
};


Model.prototype.update = async function(obj) {
  obj = obj || this.data;
  obj.slug_key = `${obj.post_type}/${obj.category||0}/${obj.slug}`;
  return await PostSchema.update(obj, { where: { id: this.data.id }}).then(r => {
    return this.data;
  });
}

Model.prototype.save = async function(obj) {
  obj = obj || this.data;
  obj.slug_key = `${obj.post_type}/${obj.category||0}/${obj.slug}`;
  return await PostSchema.create(this.data).then( r => {
    return r.dataValues ? r.dataValues : this.data;
  });
}


Model.prototype.delete = async function() {
  
  await TaxonomyRelationshipSchema.destroy({ where: { object_id: this.data.id }}).then(r => {});

  return await PostSchema.destroy({ where: { id: this.data.id }}).then(r => {
    return this.data;
  });

}


module.exports = Model;
