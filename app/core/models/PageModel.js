const { PageSchema } = require('../../core/schemas');

function Model(data) {
  this.data = data || {};
}

Model.prototype.get = async function(param) {
  if(!param){ return; }

  var obj = {};

  if( typeof param == "string" && isNaN(param) ){ obj = { slug: param }; };
  if( !isNaN(param) ){ obj = { id: param }; };
  if( typeof param == "object" ){ obj = param; };

  return await PageSchema.findOne({ where: obj }).then( page => {
    this.data = page;
    // return this.data;
  });
};

Model.prototype.getSeo = function(slug) {
  var r = this.data['seo_' + slug] || this.data[slug] || null;
  return r;
};


Model.prototype.getBreadcrumbs = function() {
  return "<ul></ul>";
};


Model.prototype.getChilds = async function(slug) {
  
  var pages =  await PageSchema.findAll({
    where: { parent: this.data.id },
    attributes: ['id', 'title', 'slug', 'parent']
  }).map( a => {
    return new Model(a);
  });

  return pages;
};

Model.prototype.getTemplate = function() {
  if( this.data.template ){
    return "pages/templates/"+this.data.template;
  }
  return "pages/index";
};

Model.prototype.update = async function(obj) {
  obj = obj || this.data;
  obj.slug_key = `${obj.parent||0}/${obj.slug}`;
  return await PageSchema.update(obj, { where: { id: this.data.id }}).then(r => {
    return this.data;
  });
}

Model.prototype.save = async function(obj) {
  obj = obj || this.data;
  obj.slug_key = `${obj.parent||0}/${obj.slug}`;
  return await PageSchema.create(this.data).then( r => {
    return this.data;
  });
}

Model.prototype.delete = async function() {
  return await PageSchema.destroy({ where: { id: this.data.id }}).then(r => {
    return this.data;
  });
}

module.exports = Model;
