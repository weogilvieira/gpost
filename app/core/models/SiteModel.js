var { ConfigSchema } = require('../../core/schemas');

function Model() {
  this.config = {};

  this.seo_default = {
    "title" : null,
    "description" : null,
    "cover" : null
  };

  this.seo = Object.assign({}, this.seo_default);

  this.getConfig = function(slug){
    if(!this.config.length){ return ""; }
    let item = this.config.find((item) => slug == item['key_slug']);
    return (item ? item['key_value'] : "");
  };


  this.setSeo = function(obj) {
    if(!obj){ obj = {}; }
    this.seo = Object.assign(this.seo_default, obj);
  }

}


Model.prototype.load = async function(){
  var r = await ConfigSchema.findAll();

  r.map( a => {
    this.config[a['key_slug']] = a['key_value'];
  })

  this.seo_default["title"] = this.config['title'];
  this.seo_default["description"] = this.config['description'];
  
  this.setSeo();

  // this.config = r;
  return r;
}

module.exports = Model;
