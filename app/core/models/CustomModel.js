var { CustomSchema } = require('../../core/schemas');

function Model(data) {
  this.data = data || {};
  if( typeof this.data.inputs != "object" ){
    this.data['inputs'] = JSON.parse(this.data.inputs || "{}");
  }
}

Model.prototype.getAll = async function(key = null, page = 1, query = null, paginate = 20) {

  if(!paginate) return await CustomSchema.findAll({ where: { key: key }, order: [[ 'id', 'DESC' ]] });

  if(page < 1){ page = 1; }
  var obj = {
    page: page,
    paginate: paginate,
    attributes: ['id','key', 'title', 'published_date', 'status'],
    order: [['id', 'DESC']],
    where: {
      key: key
    }
  };

  if(query){
    obj.where['title'] = { [sequelize.Op.like]: `%${query}%` };
  }

  try {
    await CustomSchema.paginate(obj)
  } catch(e) {
    return null;
  }
  
};

Model.prototype.get = async function(param) {
  if(!param){ return; }

  let obj = {};

  if( typeof param == "string" && isNaN(param) ){ obj = { key: param }; };
  if( !isNaN(param) ){ obj = { id: param }; };
  if( typeof param == "object" ){ obj = param; };

  return await CustomSchema.findOne({ where: obj }).then( res => {
    if(res.inputs){ res.inputs = JSON.parse(res.inputs); }
    this.data = res;
    // return this.data;
  });
};


Model.prototype.save = async function(obj) {
  
  var obj = obj || this.data;

  if( obj.inputs && typeof obj.inputs == 'object'){ obj.inputs = JSON.stringify(obj.inputs); }
  return await CustomSchema.create(this.data).then( r => {
    this.data = r;
    return this.data;
  });
}

Model.prototype.update = async function(obj) {
  if( obj.inputs && typeof obj.inputs == 'object'){ obj.inputs = JSON.stringify(obj.inputs); }
  return await CustomSchema.update(obj, { where: { id: this.data.id }}).then(r => {
    return this.data;
  });
}

Model.prototype.delete = async function() {
  return await CustomSchema.destroy({ where: { id: this.data.id }}).then(r => {
    return this.data;
  });
}

module.exports = Model;
