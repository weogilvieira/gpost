const { MediaSchema, sequelize } = require('../../core/schemas');
const fs = require("fs");

console.log(process.env.SITE_BASE_URL );

const SiteConfig = JSON.parse(
  fs.readFileSync("./config.json", "utf8")
);

function Model(data) {
  this.data = data || {};
}

Model.prototype.getAll = async function(page = 1, query = null) {
  let obj = {
    page: page,
    paginate: 24,
    attributes: ['id', 'filename'],
    order: [['id', 'DESC']]
  };

  if(query){
    obj.where = {
      filename: { [sequelize.Op.like]: `%${query.filename}%` }
    }
  }

  var res = await MediaSchema.paginate(obj);
  if( res && res.docs ){
    
    res.docs = await Promise.all(res.docs.map(async (a) => {
      let item = new Model(a);
      await item.populate();
      return item;
    }));

    return res;
  } else {
    return res;
  }

};

Model.prototype.populate = async function(){
  await this.get();
  return this.data;
}


Model.prototype.get = async function(id) {
  let res = {};
  let resUrls = {};
  let base_url = process.env.SITE_BASE_URL || "/";

  if(!this.data.id){ return; }

  let sizes = Object.keys(SiteConfig.images_sizes);
  let file = await MediaSchema.findOne({ where: { id : this.data.id || id }}).then( media => {
    return media;
  });

  if( !file ){ return null; }

  res['default'] = `${base_url}/media/${file.filename}`;
  resUrls['default'] = `${base_url}/media/${file.filename}`;

  await sizes.map( async (a) => {
    return res[a] = `${base_url}/media/${a}/${file.filename}`;
    return resUrls[SiteConfig.images_sizes[a].width] = `${base_url}/media/${a}/${file.filename}`;
  });

  this.data.sizes = res;
  this.data.urls = resUrls;
  return res;

};

Model.prototype.save = async function() {
  return await MediaSchema.create(this.data).then(r => {
    this.data = r;
    return new Model(this.data);
  })
}

Model.prototype.delete = async function(filename) {
  var sizes = ['high', 'large', 'medium', 'thumb'];

  return await MediaSchema.destroy({ where: { filename: filename }}).then( async (r) => {
    
    await fs.unlink(`./public/media/${filename}`, (err) => {  if (err) console.log(err); });

    sizes.map( a => {
      var path = `./public/media/${a}/${filename}`;
      if(fs.existsSync(path)){ fs.unlinkSync(path, (err) => { if (err) { console.log(err); } }); }
    });

    return r;
  })
}


module.exports = Model;
