const { PostTypeSchema, PostSchema, sequelize } = require('../../core/schemas');
const MediaModel = require("../../core/models/MediaModel");
const PostModel = require("../../core/models/PostModel");

function Model(data) {
  this.data = data || {};
}

Model.prototype.get = async function(param) {
  if(!param){ return; }

  let obj = {};

  if( typeof param == "string" && isNaN(param) ){ obj = { slug: param }; };
  if( !isNaN(param) ){ obj = { id: param }; };
  if( typeof param == "object" ){ obj = param; };

  return await PostTypeSchema.findOne({ where: obj }).then( postType => {
    this.data = postType;
    return postType ? new Model(this.data) : null;
  });
};

Model.prototype.getAll = async function() {
  return await PostTypeSchema.findAll().then( postTypes => {
    return postTypes;
  });
};

Model.prototype.getPosts = async function(query = {}) {
  query.post_type = this.data.id;
  const postModel = new PostModel();

  return await postModel.getAll(query).then( async(posts) => {
    return posts;
  });
};

Model.prototype.getSeo = function(slug) {
  let r = this.data['seo_' + slug] || this.data[slug] || null;
  return r;
};

Model.prototype.populatePost = async function(){
  this.data.link = await this.getLink();
  this.data.description = await this.getDescription();
  this.data.cover = await this.getCover();
  this.data.category = await this.getCategory();
  this.data.tags = await this.getTags();
  this.data.author = await this.getAuthor();
  return this.data;
}


Model.prototype.getCover = async function(size = false){
  if(!this.data.cover){ return ""; }
  let media = new MediaModel();
  var mediaObj = await media.get(this.data.cover);

  if(!mediaObj){ return; }
  
  return size ? `${mediaObj[size]}` : mediaObj;
}


Model.prototype.getDescription = async function(){
  return this.data.description ? this.data.description : this.data.content;
};

Model.prototype.getLink = async function(base = ""){
  let cat = new TaxonomyModel();
  await cat.get(this.data.category);

  return `${base}//${this.data.slug}`;
};

Model.prototype.getTags = async function(){
  var taxsModel = new TaxonomyRelationshipModel();
  let taxs = await taxsModel.getAll(this.data.id);
  let tagsIds = [];
  taxs.map(a => tagsIds.push(a.taxonomy_id) );
  let tagsModel = new TaxonomyModel();
  let tags = await tagsModel.getRange(tagsIds, 'tag');
  
  tags = await Promise.all(tags.map(async (a) => {
    return new TaxonomyModel(a);
  }));

  return tags;
};


Model.prototype.update = async function(obj) {
  obj = obj || this.data;
  obj.slug_key = `${obj.post_type}/${obj.category||0}/${obj.slug}`;
  return await PostTypeSchema.update(obj, { where: { id: this.data.id }}).then(r => {
    return this.data;
  });
}

Model.prototype.save = async function(obj) {
  obj = obj || this.data;
  obj.slug_key = `${obj.post_type}/${obj.category||0}/${obj.slug}`;
  return await PostTypeSchema.create(this.data).then( r => {
    return r.dataValues ? r.dataValues : this.data;
  });
}


Model.prototype.delete = async function() {
  
  return await PostTypeSchema.destroy({ where: { id: this.data.id }}).then(r => {
    return this.data;
  });

}

module.exports = Model;
