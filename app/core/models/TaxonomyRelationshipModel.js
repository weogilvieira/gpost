const slugify = require('slugify');
const { TaxonomyRelationshipSchema } = require('../../core/schemas');

function Model(data) {
  this.data = data || {};
}

Model.prototype.getAll = async function(object_id = null, taxonomy_id = null) {
  let where_obj = {};
  
  if( object_id ) where_obj['object_id'] = object_id;
  if( taxonomy_id ) where_obj['taxonomy_id'] = taxonomy_id;

  let obj = {
    attributes: ['id','taxonomy_id', 'object_id', 'post_type'],
    order: [['id', 'DESC']],
    where: where_obj
  };

  return await TaxonomyRelationshipSchema.findAll(obj);
};


Model.prototype.get = async function(param) {
  if(!param){ return; }

  let obj = {};

  if( typeof param == "string" && isNaN(param) ){ obj = { slug: param }; };
  if( !isNaN(param) ){ obj = { id: param }; };
  if( typeof param == "object" ){ obj = param; };

  return await TaxonomyRelationshipSchema.findOne({ where: obj }).then( res => {
    this.data = res;
  });
};


Model.prototype.update = async function(obj) {

  if( obj ){ this.data = obj; }


  return await TaxonomyRelationshipSchema.update({ title: this.data.title, slug: this.data.slug }, { where: { id: this.data.id }}).then(r => {
    this.data = r;
    return this.data;
  });
}

Model.prototype.save = async function(obj) {
  
  if( obj ){ this.data = obj; }

  if(!this.data){ return; }

  var _self = this;

  return await TaxonomyRelationshipSchema.create(this.data).then( r => {
    _self.data = r;
    return _self.data;
  });
}


Model.prototype.delete = async function() {
  return await TaxonomyRelationshipSchema.destroy({ where: { id: this.data.id }}).then(r => {
    return this.data;
  });
}

module.exports = Model;
