"use strict"
const sequelizePaginate = require('sequelize-paginate');

module.exports = function(sequelize, DataTypes) { 
  // Define resource
  var Media = sequelize.define('media', {
    filename: {
      type: DataTypes.STRING,
      allowNull: false
    }
  });

  Media.sync();
  
  sequelizePaginate.paginate(Media);
  return Media;
}
