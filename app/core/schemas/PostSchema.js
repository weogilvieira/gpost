"use strict"
const sequelizePaginate = require('sequelize-paginate');

module.exports = function(sequelize, DataTypes) { 
  // Define resource
  var Post = sequelize.define('post', {
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING
    },
    cover: {
      type: DataTypes.STRING
    },
    published_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    highlighted: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    content: {
      type: DataTypes.TEXT
    },
    slug: {
      type: DataTypes.STRING,
      allowNull: false
    },
    post_type: {
      type: DataTypes.STRING,
      defaultValue: "blog"
    },
    category: {
      type: DataTypes.STRING,
      allowNull: false
    },
    author: {
      type: DataTypes.STRING,
      allowNull: false
    },
    seo_title: {
      type: DataTypes.STRING
    },
    seo_description: {
      type: DataTypes.STRING
    },
    custom_fields: {
      type: DataTypes.TEXT
    },
    slug_key: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    }
  });

  Post.sync();
  sequelizePaginate.paginate(Post);
  return Post;
}
