"use strict"

module.exports = function(sequelize, DataTypes) { 
  // Define resource
  var Page = sequelize.define('page', {
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
    cover: {
      type: DataTypes.UUID
    },
    slug: {
      type: DataTypes.STRING,
      allowNull: false
    },
    page_hook: {
      type: DataTypes.STRING
    },
    parent: {
      type: DataTypes.UUID
    },
    content: {
      type: DataTypes.TEXT
    },
    template: {
      type: DataTypes.STRING,
    },
    seo_title: {
      type: DataTypes.STRING
    },
    seo_description: {
      type: DataTypes.STRING
    },
    custom_fields: {
      type: DataTypes.TEXT
    },
    slug_key: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    }
  });


  Page.sync();
  // Page.sync({force: true}).then( () => {
  //   Page.create({
  //     title: "Contato",
  //     description: "Contato Description Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, nostrum.",
  //     slug: "contato"
  //   });

  //   Page.create({
  //     title: "Sobre",
  //     description: "Contato Description Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, nostrum.",
  //     slug: "sobre"
  //   });

  // });
  

  return Page;
}
