"use strict"
const sequelizePaginate = require('sequelize-paginate');
module.exports = function(sequelize, DataTypes) { 
  // Define resource
  var Form = sequelize.define('form', {
    key: {
      type: DataTypes.STRING
    },
    inputs: {
      type: DataTypes.TEXT
    }
  });

  Form.sync();
  sequelizePaginate.paginate(Form);
  return Form;
};
