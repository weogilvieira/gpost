"use strict"
const sequelizePaginate = require('sequelize-paginate');
module.exports = function(sequelize, DataTypes) { 
  // Define resource
  var Taxonomy = sequelize.define('taxonomy', {
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    slug: {
      type: DataTypes.STRING,
      allowNull: false
    },
    target: {
      type: DataTypes.STRING,
      defaultValue: "post"
    },
    type: {
      type: DataTypes.STRING,
      defaultValue: "category"
    },
    slug_key: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    }
  });

  Taxonomy.sync();

  sequelizePaginate.paginate(Taxonomy);
  return Taxonomy;
}
