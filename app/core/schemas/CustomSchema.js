"use strict"
const sequelizePaginate = require('sequelize-paginate');

module.exports = function(sequelize, DataTypes) { 
  // Define resource
  const Custom = sequelize.define('custom', {
    key: {
      type: DataTypes.STRING,
      allowNull: false
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    inputs: {
      type: DataTypes.STRING
    },
    published_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  });


  // Custom.sync({ force: true }).then(() => {
  //   // Table created
  //   return Custom.create({
  //     key: "stores",
  //     title: "Blastoise",
  //     inputs: "{\"type\":\"comercio\",\"location\":\"-121212, -12120\",\"address\":\"lorem ipsum dolor\"}",
  //   });
  // });

  sequelizePaginate.paginate(Custom);
  return Custom;
}
