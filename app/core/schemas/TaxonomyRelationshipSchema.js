"use strict"
module.exports = function(sequelize, DataTypes) { 
  // Define resource
  var TaxonomyRelationship = sequelize.define('taxonomy_relationship', {
    taxonomy_id: {
      type: DataTypes.STRING,
      allowNull: false
    },
    object_id: {
      type: DataTypes.STRING,
      allowNull: false
    },
    post_type: {
      type: DataTypes.STRING
    }
  });

  TaxonomyRelationship.sync();

  return TaxonomyRelationship;
}
