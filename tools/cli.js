"use strict";
const fs = require("fs");
const path = require('path');
const args = process.argv.slice(2);
const inquirer = require('inquirer');

const methods = {};

fs.readdirSync(`${__dirname}/scripts`).forEach(function(filename) {
  if(filename.indexOf('.js')==-1){ return; }
  methods[filename.replace(".js", "").toLowerCase()] = path.join(__dirname, './scripts/', filename);
});

const questions = [
  {
    name: 'method',
    type: 'list',
    message: 'Escolha uma ação:',
    choices: Object.keys(methods)
  }
];

if(!args.length || !methods[args[0]]){
  
  if( !methods[args[0]] ) {
    console.log("Método não encontrado.\n");
  }

  inquirer.prompt(questions).then(res => {
    return require(methods[res.method]);
  });

} else if(args.length){

  if( !methods[args[0]] ){ return; }
  return require( methods[args[0]] );

}
