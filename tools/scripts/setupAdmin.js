"use strict";
const CLI = require('clui');
const inquirer = require('inquirer');
const Spinner = CLI.Spinner;

const questions = [
  {
    name: 'name',
    type: 'input',
    message: '[Admin] Nome do usuário',
    default: "admin",
    validate: function( value ) {
      if (value.length) {
        return true;
      } else {
        return 'Campo requerido.';
      }
    }
  },
  {
    name: 'email',
    type: 'input',
    message: '[Admin] E-mail do usuário',
    default: "admin@admin.com",
    validate: function( value ) {
      if (value.length && value.indexOf("@")!==-1 && value.indexOf(".")!==-1 ) {
        return true;
      } else {
        return 'Campo requerido.';
      }
    }
  },
  {
    name: 'password',
    type: 'input',
    message: '[Admin] Senha do usuário',
    default: "admin@admin",
    validate: function( value ) {
      if (value.length) {
        return true;
      } else {
        return 'Campo precisa ter 6 caracteres.';
      }
    }
  },
];

module.exports = new Promise(async(reject, resolve) => {
  const { UserSchema, sequelize } = require("../../app/core/schemas.js");

  const checking = new Spinner('Checando existência de usuários...');
  checking.start();

  var usersData = await UserSchema.findAll();

  if(usersData && usersData.length ){
    console.log("Já existem usuários na plataforma.\n");
    checking.stop();
    resolve(true);
    return;
  }

  checking.stop();

  var user = {
    name: '',
    role: 'dev',
    front_role: 'Admin',
    email: '',
    password: '',
    bio: '',
    active: true
  };

  var questionsResponses = await inquirer.prompt(questions);

  Object.keys(questionsResponses).map(a => {
    user[a] = questionsResponses[a];
  });

  const saving = new Spinner('Salvando usuário...');
  saving.start();

  await UserSchema.create(user)
  .then(r => {
    return true;
  })
  .catch(err => {
    throw new Error(err);
  })

}).catch(e => {
  if(!e){  throw new Error(e); }
  return e;
});
