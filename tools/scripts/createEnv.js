"use strict";
const chalk = require('chalk');
const inquirer = require('inquirer');
const Sequelize = require('sequelize');
const fs = require("fs");

const questions = [
  {
    name: 'NODE_ENV',
    type: 'list',
    message: '[APP] Tipo de ambiente',
    default: "development",
    choices: ["development", "production"],
    validate: function( value ) {
      if (value.length) {
        return true;
      } else {
        return 'Tipo de ambiente é requerido.';
      }
    }
  },
  {
    name: 'PORT',
    type: 'input',
    message: '[APP] Porta',
    default: "3000",
    validate: function( value ) {
      if (value.length) {
        return true;
      } else {
        return 'Host MariaDB é requerido.';
      }
    }
  },
	{
		name: 'MYSQL_URL',
    type: 'input',
    message: '[MariaDB] Host',
    default: 'localhost',
    validate: function( value ) {
      if (value.length) {
        return true;
      } else {
        return 'Host MariaDB é requerido.';
      }
    }
	},
  {
    name: 'MYSQL_DATABASE',
    type: 'input',
    message: '[MariaDB] Database',
    validate: function( value ) {
      if (value.length) {
        return true;
      } else {
        return 'Database é requerido.';
      }
    }
  },
  {
    name: 'MYSQL_USER',
    type: 'input',
    default: 'root',
    message: '[MariaDB] Usuário',
    validate: function( value ) {
      if (value.length) {
        return true;
      } else {
        return 'Usuário é requerido.';
      }
    }
  },
  {
    name: 'MYSQL_PASS',
    type: 'input',
    message: '[MariaDB] Senha',
    validate: function( value ) {
      if (value.length) {
        return true;
      } else {
        return 'Senha é requerido.';
      }
    }
  }
];


const writeEnvFile = async ( data, cb ) => {
  var envContent = "";

  Object.keys(data).map(a => {
    envContent+= `${a} = "${data[a]}"\n`;
  });

  fs.writeFile('.env', envContent, function (err) {
    if (err) throw err;
    console.log('Arquivo ".env" criado com sucesso!\n\n');
    return cb(true);
  });
};



module.exports = new Promise(async(resolve, reject) => {

  if( fs.existsSync('./.env') ) {
    console.log("O Arquivo .env já existe.\n\n");
    resolve(true);
    return;
  }

  let envFiles = [];

  fs.readdirSync(`./`).forEach(function(filename) {
    if(filename.indexOf('.env')==-1){ return; }
    envFiles.push(filename)
  });

  console.log(envFiles, envFiles.length);

  if( envFiles.length ) {

    let envQuestions = {
      name: 'envOption',
      type: 'list',
      message: 'Deseja copiar as configurações de outro arquivo ou criar um novo?',
      default: "Criar Novo",
      choices: ['Criar Novo'].concat(envFiles)
    };

    let envQuestionsResponse = await inquirer.prompt(envQuestions);

    if( envQuestionsResponse.envOption != "Criar Novo" ){
      console.log("Copiar arquivo.", envQuestionsResponse.envOption);
      fs.copyFileSync(`./${envQuestionsResponse.envOption}`, './.env')
      return false;
    }
  }


  var questionsResponses = await inquirer.prompt(questions);
      questionsResponses['SECRET'] = "GPOSTCMS";


  const questionBASEURL = [
    {
      name: 'SITE_BASE_URL',
      type: 'input',
      message: '[APP] URL do site',
      default: `http://localhost:${questionsResponses.PORT}`,
      validate: function( value ) {
        if (value.length) {
          return true;
        } else {
          return 'Tipo de ambiente é requerido.';
        }
      }
    }
  ]

  questionsResponses["SITE_BASE_URL"] = `http://localhost:${questionsResponses.PORT}`;

  if( questionsResponses.NODE_ENV == 'production' ){
    var responseBASEURL = await inquirer.prompt(questionBASEURL);
    questionsResponses.SITE_BASE_URL = responseBASEURL.SITE_BASE_URL;
  }

 
  const sequelize = new Sequelize(questionsResponses.MYSQL_DATABASE, questionsResponses.MYSQL_USER, questionsResponses.MYSQL_PASS, {
    host: questionsResponses.MYSQL_URL,
    dialect: 'mysql',
    operatorsAliases: false,
    logging: false,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    query: { 
      raw:true 
    }
  });

  return new Promise((resolve, reject) => {
    sequelize
      .authenticate()
      .then( async () => {
        console.log('Connection has been established successfully.');
        await writeEnvFile(questionsResponses, async (res) => {
          resolve(true);
        });
      })
      .catch(err => {
        reject(new Error(`Unable to connect to the database: ${err}`));
      });
  });
}).catch(e => {
  if(!e){  throw new Error(e); }
  return e;
});;
