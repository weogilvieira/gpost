"use strict";
const CLI = require('clui');
const inquirer = require('inquirer');
const Spinner = CLI.Spinner;

const questions = [
  {
    name: 'sitename',
    type: 'input',
    message: '[Site] Titulo do site (pode ser alterado depois)',
    default: "Titulo do Site",
    validate: function( value ) {
      if (value.length) {
        return true;
      } else {
        return 'Campo requerido.';
      }
    }
  },
  {
    name: 'description',
    type: 'input',
    message: '[Site] Descrição padrão do site (SEO)',
    default: "Descrição do site.",
    validate: function( value ) {
      if (value.length) {
        return true;
      } else {
        return 'Campo requerido.';
      }
    }
  }
];

module.exports = new Promise(async(reject, resolve) => {


  const { ConfigSchema, sequelize } = require("../../app/core/schemas.js");

  const checking = new Spinner('Checando existência das configurações...');
  checking.start();

  var configData = await ConfigSchema.findAll({'atributes' : ['id']});

  if(configData && configData.length ){
    console.log("\nConfigurações do site já foram atribuidas anteriormente.\nUse o painel admin para atualiza-las.\n");
    checking.stop();
    resolve(true)
    return;
  }

  checking.stop();

  var configs = [];
  var questionsResponses = await inquirer.prompt(questions);

  Object.keys(questionsResponses).map(a => {
    
    configs.push(
      new Promise( async (rej, res) => {
        ConfigSchema.create({
          key_value: questionsResponses[a],
          key_name: (a == 'sitename' ? "Nome do Site" : "Descrição do site"), 
          key_slug: a
        }).then(r => {
          res(r);
        }).catch(e => {
          rej(e);
        });
      })
    );

  });

  const saving = new Spinner('Salvando configurações...');
  saving.start();

  return await Promise.all(configs).then(function(res){
    saving.stop();
    resolve();
  }).catch(err => {
    saving.stop();
    reject();
  });

}).catch(e => {
  if(!e){  throw new Error(e); }
  return e;
});;
