"use strict";
const CLI = require('clui');
const inquirer = require('inquirer');
const Spinner = CLI.Spinner;

const questions = [
  {
    name: 'sitename',
    type: 'input',
    message: '[Site] Titulo do site (pode ser alterado depois)',
    default: "Titulo do Site",
    validate: function( value ) {
      if (value.length) {
        return true;
      } else {
        return 'Campo requerido.';
      }
    }
  },
  {
    name: 'description',
    type: 'input',
    message: '[Site] Descrição padrão do site (SEO)',
    default: "Descrição do site.",
    validate: function( value ) {
      if (value.length) {
        return true;
      } else {
        return 'Campo requerido.';
      }
    }
  }
];

module.exports = new Promise(async(reject, resolve) => {


  const { PostTypeSchema, sequelize } = require("../../app/core/schemas.js");

  var postTypes = await PostTypeSchema.findAll();

  if(postTypes && postTypes.length ){
    console.log("\nJá existem post types no site.\nUse o painel admin para atualiza-las.\n");
    resolve(true)
    return;
  }

  return await PostTypeSchema.create({
    title: 'Posts',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel, sint.',
    slug: 'posts'
  }).then(r => {
    resolve(true);
  }).catch(e => {
    reject(e);
  });

  return;

}).catch(e => {
  if(!e){  throw new Error(e); }
  return e;
});;
