"use strict";
const fs = require('fs');
require('dotenv').config();


module.exports = new Promise(async(reject, resolve) => {

  var envCMS = `export const environment = {
    production: false,
    site_url: "${process.env.SITE_BASE_URL}"
  };`

  var actualEnv = fs.readFileSync(`app_cms/src/environments/environment.dev.ts`, 'utf8');

  if( envCMS !== actualEnv ) {
    fs.writeFile(`app_cms/src/environments/environment.dev.ts`, envCMS, function (err) {
      if (err) throw err;
      console.log('Arquivo de configuração do admin atualizado com sucesso!\n\n');
    });
  }

}).catch(e => {
  if(!e){
    throw new Error(e)
  };
  return e;
})

