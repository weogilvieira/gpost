"use strict";
const chalk = require('chalk');
const inquirer = require('inquirer');
const figlet = require('figlet');
const pjson = require('../../package.json');

const runInstaller = async () => {
	
	console.log("\nBem vindo ao instalador.");
	
  console.log(
	  chalk.yellow(
	    figlet.textSync('GPOST', { horizontalLayout: 'full' })
	  )
	);
  console.log(`Versão: ${pjson.version}`);
  console.log(`Por ${pjson.author}`);


  return Promise.resolve()
    .then(() => {
      return require('./createEnv');
    })
    .then(() => {
      return require('./setupSite');
    })
    .then(() => {
      return require('./setupPosttypes');
    })
    .then(() => {
      return require('./setupAdmin');
    })
    .then(() => {
      console.log("---- FIM DO INSTALADOR ----");
      console.log("Reinicie a aplicação.");
      process.exit();
    }).catch(e => {
      console.log(e);
    });
};

module.exports = new Promise(async (resolve, reject) => {
  await inquirer
    .prompt([
  	  {
  	  	type: "confirm",
  	  	name: "runInstaller",
  	    message: "Deseja iniciar o instalador?"
  	  }
    ])
    .then(answers => {
    	if(!answers.runInstaller){ return; }
    	return runInstaller();
    });

})



