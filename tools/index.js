"use strict";
const fs = require("fs");
const path = require('path');
const args = process.argv.slice(2);
const inquirer = require('inquirer');

const methods = {};

fs.readdirSync(`${__dirname}/scripts`).forEach(function(filename) {
  if(filename.indexOf('.js')==-1){ return; }
  methods[filename.replace(".js", "").toLowerCase()] = path.join(__dirname, './scripts/', filename);
});

module.exports = async (method) => {
  if( !methods[method] ){ return; }
  return require(methods[method]);
}


