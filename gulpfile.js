'use strict';
const gulp = require('gulp');
const fs = require('fs');
const shelljs = require('shelljs');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const uglify = require('gulp-uglify-es').default;
const sourcemaps = require('gulp-sourcemaps');
const log = require('gulplog');
const wait = require('gulp-wait');
const imagemin = require('gulp-imagemin');
const imageminWebp = require('imagemin-webp');
const dotenv = require('dotenv');
dotenv.config({ path: 'prod.env' });

gulp.task('prepare-build', () => {
  shelljs.rm('-rf', 'build/');
  shelljs.mkdir('build/');
  shelljs.exec('cp prod.env build/.env');
});

gulp.task('scripts', () => {

  var b = browserify({
    entries: 'app/src/js/script.js',
    insertGlobals : true,
    debug : true
  })

  return b.bundle()
    .pipe(source('script.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(uglify())
      .on('error', log.error)
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest( 'app/public/assets/js'));
}); //scritps

gulp.task('build-admin', () => {

  shelljs.exec('cd app_cms/ && npm install && npm run build');

  return gulp.src('app_cms/dist/admin/**/*')
    .pipe(gulp.dest('build/admin'));

});


gulp.task('imagemin', () => {
  return gulp.src('app/src/img/**/*.{gif,png,jpg,svg}')
    .pipe(wait(500))
    .pipe(imagemin({
      interlaced: true,
      progressive: true,
      optimizationLevel: 5,
      use: [
        imageminWebp({ quality: 50 })
      ]
    }))
    .pipe(gulp.dest('app/public/assets/img'));
});//imagemin

gulp.task('copy-to-build-folder', () => {

  return gulp
    .src([
      'package.json',
      'package-lock.json',
      'app/**/*',
      '!app/src',
      '!app/src/**/*'
    ])
    .pipe(gulp.dest( 'build/' ))
});


gulp.task('build', ['prepare-build', 'scripts', 'imagemin', 'copy-to-build-folder', 'build-admin'], () => {
  console.log("build complete");
})
