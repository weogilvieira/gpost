"use strict";
const fs = require("fs");

if( !fs.existsSync('./.env') ) {
  console.log(`O arquivo ".env" não foi encontrado.`);
	return require("./tools")('install');
}

require("./tools")('setcms');

return require("./app");
