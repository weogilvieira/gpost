import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class Helper {

  constructor(
    private router: Router
  ){}

  slugify(str) {
      str = str.replace(/^\s+|\s+$/g, ''); // trim
      str = str.toLowerCase();

      // remove accents, swap ñ for n, etc
      var from = "õãàáäâèéëêìíïîòóöôùúüûñç·/_,:;";
      var to   = "oaaaaaeeeeiiiioooouuuunc------";
      for (var i=0, l=from.length ; i<l ; i++) {
          str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
      }

      str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
          .replace(/\s+/g, '-') // collapse whitespace and replace by -
          .replace(/-+/g, '-'); // collapse dashes

      return str;
  }

  logout(){
    localStorage.clear();
    this.router.navigateByUrl("login");
  }

  getConfigByKey(siteConfigKeys: any = [], key) {
    let found = siteConfigKeys.find((a) => {
      return a.key_slug == key;
    });
    return found.key_value;
  };
}
