export const environment = {
    production: true,
    site_url: (function(){
      return `${window.location.protocol}//${window.location.host}`;
    })()
  };
