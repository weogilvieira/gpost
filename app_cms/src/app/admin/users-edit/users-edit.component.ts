import { Component, OnInit, ElementRef, ViewChild, Injector  } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../service/user.service';
import { SiteService } from '../../service/site.service';

@Component({
  selector: 'app-users-edit',
  templateUrl: './users-edit.component.html',
  styleUrls: ['./users-edit.component.scss']
})
export class UsersEditComponent implements OnInit {

  USER: any = null;
  MODE: string = "new";
  ID: string = "";
  loaded: boolean = false;
  siteConfig: any = {};
  ROLES: any = ['editor', 'admin', 'dev'];

  form = new FormGroup({
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    bio: new FormControl(''),
    front_role: new FormControl(''),
    role: new FormControl('editor'),
    active: new FormControl(1),
    passwordA: new FormControl(''),
    passwordB: new FormControl('')
  });

  constructor(
    private userService: UserService,
    private siteService: SiteService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title
  ) { }

  ngOnInit() {

    this.activatedRoute.data.subscribe((data) => {
      if( data.mode ){ this.MODE = data.mode; }
    });

    this.titleService.setTitle(this.MODE == "new" ? `Novo Usuário` : `Editar Usuário`);

    if( this.MODE == "edit" ){
      this.ID = this.activatedRoute.snapshot.paramMap.get('id');

      this.userService.get(this.ID)
      .subscribe( res => {
        this.USER = res.data;
        this.form.patchValue(this.USER);
        this.loaded = true;
      });

    } else {
      this.loaded = true;
    }
  }//ngOnInit


  save(form, exit: boolean = false) {

    if(!form.valid){ alert("Alguns campos são obrigatórios."); }

    let obj = Object.assign({}, form.value);

    if( obj.email.indexOf('@')==-1 || obj.email.indexOf('.')==-1 ) {

      alert("E-mail inválido.");
      return;

    }

    if( this.MODE == 'new' && !obj.passwordA.length ){
      alert("Senha é obrigatória.");
      return;
    };


    if( obj.passwordA.length && (obj.passwordA == obj.passwordB) ){
      obj['password'] = obj.passwordA;

      if( obj['password'].length < 6 ) {
        alert("Senha precisa ter 6 caracteres.");
        return;
      }
    }

    if( obj.passwordA.length  && (obj.passwordA != obj.passwordB) ){
      alert("As senhas são diferentes.");
      return;
    }

    delete obj.passwordA;
    delete obj.passwordB;

    this.userService[(this.MODE == 'edit') ? 'put' : 'post'](obj, this.ID)
      .subscribe(res => {

        if( res.message ){
          alert(res.message);
        }

        if( res.data ){
          return this.router.navigateByUrl("/users");
        }

      }, error => {
        let msg = "Erro ao "+(this.MODE != 'edit' ? 'registrar' : 'atualizar')+", tente novamente.";
        alert(error.message ? error.message : msg);
      });
  };

  cancel(){
    var confirm = window.confirm("Deseja sair sem salvar?");
    if(confirm){
      return this.router.navigateByUrl("/users");
    }
  }

  delete() {
    var confirm = window.confirm("Deseja mesmo deletar este usuário?");
    if(confirm){

      return this.userService.delete(this.ID)
        .subscribe( res => {
          if( res.message ){
            alert(res.message);
          }

          if( res.data ){
            alert("Usuário deletado com sucesso.");
            return this.router.navigateByUrl("/users");
          }
        })

    }
  }

}
