import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { SiteService } from '../../service/site.service';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  data: any = {};
  postType: any = {};
  loaded: boolean = false;
  siteConfig: any = {};
  PAGE: number = 1;
  query = new FormControl('');

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private siteService: SiteService,
    private userService: UserService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.load()
  }


  load(page = null) {
    this.loaded = false;
    this.PAGE = page || this.PAGE;
    var obj = {};

    if(this.query.value) {
      obj['query'] = this.query.value;
    }

    this.userService.getAll(obj, this.PAGE)
      .subscribe( res => {
        this.data = res.data;
        this.loaded = true;
      });
  }

  resetSearch() {
    if(!this.query.value){ return; }
    this.query.setValue('');
    this.load(1);
  }

}
