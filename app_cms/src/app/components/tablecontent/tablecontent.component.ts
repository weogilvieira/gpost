import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tablecontent',
  templateUrl: './tablecontent.component.html',
  styleUrls: ['./tablecontent.component.scss']
})
export class TablecontentComponent implements OnInit {

  @Input() columns : any = [];
  @Input() data : any = [];

  constructor() { }

  ngOnInit() {
  }

}
