import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges, SimpleChange } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss']
})
export class SearchbarComponent implements OnInit, OnChanges {

  @Input() filters;
  @Input() value;
  private _filters: any = [];
  @Output() onSearch = new EventEmitter();
  @Output() onReset = new EventEmitter();

  form: any = null;

  constructor() { }

  ngOnInit() {
    this.buildForm();
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.filters && !!changes.filters.currentValue.length && changes.filters.previousValue != changes.filters.currentValue){
      this._filters = this.filters;
      this.buildForm();
    }
  }

  buildForm() {
    const group = {
      query: new FormControl('')
    };

    if( this._filters ) {
      this._filters.forEach( item => {
        group[item.name] = new FormControl('');
      });
    };

    this.form = new FormGroup(group);

    this.form.patchValue(this.value||{});
  }

  submit() {
    if(!this.form.valid){ return; }
    this.onSearch.emit(this.form.value);
  }

  reset() {
    this.onReset.emit();
  }


}
