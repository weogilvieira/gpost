import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'page-head',
  templateUrl: './head.component.html',
  styleUrls: ['./head.component.scss']
})
export class HeadComponent implements OnInit {

  @Input() title : string = "";
  @Input() counter : string = "";
  @Output() onCancel = new EventEmitter();
  @Output() onSave = new EventEmitter();
  @Output() onEdit = new EventEmitter();
  @Output() onNew = new EventEmitter();

  showSave: boolean = false;
  showCancel: boolean = false;
  showEdit: boolean = false;
  showNew: boolean = false;

  constructor(private titleService: Title) {
  }

  ngOnInit() {
    this.showSave = !!this.onSave.observers.length;
    this.showCancel = !!this.onCancel.observers.length;
    this.showEdit = !!this.onEdit.observers.length;
    this.showNew = !!this.onNew.observers.length;
    this.titleService.setTitle(`${this.title}`);
  }

  runAction( action ) {
    switch (action) {
      case "cancel":
        this.onCancel.emit();
        break;
      case "save":
        this.onSave.emit();
        break;
      case "edit":
        this.onEdit.emit();
        break;
      case "new":
        this.onNew.emit();
        break;

      default:
        console.log(`Nenhuma ação configurada para ${action}.`);
        break;
    }
  }

}
