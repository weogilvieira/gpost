import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Input() page: number = 1;
  @Input() total: number = 1;
  @Input() limit: number = 5;



  @Output() changepage = new EventEmitter();
  pageArray: any = [];


  constructor() { }

  ngOnInit() {

    let offset = (this.limit/2);
    this.pageArray = [];
    this.pageArray.push(this.page);

    for(let i = 1; i < offset; i++) {
      if( this.page-i < 1){ break; }
      this.pageArray.push(this.page-i);
    }

    this.pageArray.sort();

    offset = (this.limit-this.pageArray.length);

    for(let i = 1; i <= offset; i++) {
      if( this.page+i > this.total){ break; }
      this.pageArray.push(this.page+i);
    }
  }

  setPage( pg = 1 ){

    if( pg > this.total || pg < 1 || pg == this.page){ return; }
    this.changepage.emit(pg);
  }

}
