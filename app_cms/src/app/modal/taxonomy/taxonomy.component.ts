import { Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import { TaxonomyService } from '../../service/taxonomy.service';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-taxonomy',
  templateUrl: './taxonomy.component.html',
  styleUrls: ['./taxonomy.component.scss']
})
export class TaxonomyComponent implements OnInit {
  SITE_URL = environment.site_url;
  items: any = [];
  loaded: boolean = true;
  PAGE: any = 1;

  form = new FormGroup({
    title: new FormControl('', [Validators.required, Validators.minLength(3)])
  });

  @Input() target;
  @Input() type;
  @Output() outputData = new EventEmitter();
  @Output() dismissAction = new EventEmitter();

  constructor(
    private taxonomyService: TaxonomyService
    ) { }

  ngOnInit() {
    this.loadData();
  }


  dismiss(){
    return this.dismissAction.emit();
  }

  selectItem( item ) {
    this.outputData.emit(item);
    this.dismiss();
  }

  create( form ) {
    if(!form.valid){ alert("Nome da categoria vázio ou muito curto."); }
    let obj = form.value;

    let _self = this;

    this.taxonomyService.post(this.target, this.type, obj)
      .subscribe( res => {
        if( res.message ){ alert(typeof res.message == 'string' ? res.message : 'Algo errado. Tente novamente ou entre em contato com o suporte.'); return; }
        _self.loadData(1);
        _self.form.reset();
      });
  }

  update( item ) {
    if(item.title.length <= 3){ alert("Nome da categoria vázio ou muito curto."); }
    let obj = item;
    let _self = this;

    this.taxonomyService.put(obj.id, obj)
      .subscribe( res => {
        if( res.message ){ alert(typeof res.message == 'string' ? res.message : 'Algo errado. Tente novamente ou entre em contato com o suporte.'); return; }
        _self.loadData();
        _self.form.reset();
      });
  }

  delete(id) {
    if(!id){ return; }
    var confirm = window.confirm("Deseja mesmo deletar este item?");
    if(!confirm){ return; }

    return this.taxonomyService.delete(id)
      .subscribe( res => {
        if( res.message ){
          alert(typeof res.message == 'string' ? res.message : 'Algo errado. Tente novamente ou entre em contato com o suporte.');
          return;
        }

        if( res.data ){
          alert("Item deletado com sucesso.");
          this.loadData();
        }
      })

  }

  loadData(page = null) {
    this.PAGE = page || this.PAGE;
    this.loaded = false;

    this.taxonomyService.getAll(this.target, this.type)
      .subscribe( res => {
        this.items = res.data;
        this.loaded = true;
      });
  }
}
