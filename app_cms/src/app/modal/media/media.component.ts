import { Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import { MediaService } from '../../service/media.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.scss']
})
export class MediaComponent implements OnInit {
  SITE_URL = environment.site_url;
  medias: any = [];
  loaded: boolean = true;
  form: FormGroup;
  isUploading: boolean = false;
  imageIsValid: boolean = false;
  query = new FormControl('');
  PAGE: any = 1;

  @Output() imageData = new EventEmitter();
  @Output() dismissAction = new EventEmitter();
  @ViewChild('fileInput', { static: false }) fileInput: ElementRef;

  constructor(
    private mediaService: MediaService,
    private fb: FormBuilder
    ) {
      this.createForm();
    }

  createForm() {
    this.form = this.fb.group({
      image: null
    });
  }


  ngOnInit() {
    this.loadMedia();
  }

  dismiss() {
    this.dismissAction.emit();
  }


  selectImage(obj) {
    console.log(obj);
    this.imageData.emit(obj.data || {});
    this.dismiss();
  };


  delete(filename) {
    var r = confirm("Deseja excluir?");
    if (!r) { return; }

    this.mediaService.delete(filename)
      .subscribe( res => {
        if( res && !res.message ){
          this.loadMedia(this.PAGE);
        }
      });
  }


  resetBusca() {
    if(!this.query.value){ return; }
    this.query.setValue('');
    this.loadMedia();
  }

  onFileChange(event) {
    this.imageIsValid = false;

    if(event.target.files.length > 0) {
      let file = event.target.files[0];

      if( file.size > 1000000 ){
        alert("Este arquivo é maior que 1Mb.")
        this.form.get('image').setValue(null);
        return;
      }
      this.imageIsValid = true;
      this.form.get('image').setValue(file);
    }
  }

  private prepareSave(): any {
    let input = new FormData();
    input.append('upload', this.form.get('image').value);
    return input;
  }

  upload($event) {
    if( !this.imageIsValid ){
      alert("Algo errado, verifique o formato e tamanho do arquivo.");
      return;
    }

    this.isUploading = true;
    const formModel = this.prepareSave();
    this.mediaService.post(formModel)
      .subscribe( res => {
        if( res.data ){
          this.clearFile()
          this.isUploading = false;
          this.loadMedia();
        }
      });

    return;
  };

  clearFile() {
    this.form.get('image').setValue(null);
    this.fileInput.nativeElement.value = '';
    this.imageIsValid = false;
  }

  loadMedia(page = null) {
    this.PAGE = page || this.PAGE;

    this.loaded = false;

    let obj = {};
    if(this.query.valid){
      obj['filename'] = this.query.value
    }

    this.mediaService.getAll(obj, this.PAGE)
      .subscribe( res => {
        this.medias = res.data;
        this.loaded = true;
      });
  }


}
