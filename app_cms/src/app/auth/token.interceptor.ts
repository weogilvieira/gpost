import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpResponse,
  HttpErrorResponse,
  HttpInterceptor
} from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(
    public auth: AuthService,
    private toastr: ToastrService
  ) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    request = request.clone({
      setHeaders: {
        'x-access-token': `${this.auth.getToken()}`
      }
    });
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        let err = (typeof error.error == 'string' && error.error ? error.error : null ) || error.message || "Erro, reporte o desenvolvedor.";
          this.toastr.error('Erro', err);
          return throwError(error);
      })
    );
  }
}
