import { Injectable } from '@angular/core';
import decode from 'jwt-decode';
import { map } from 'rxjs/operators';
import { Http } from '@angular/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: Http
    ) {
  }

  public getToken(): string {
    return localStorage.getItem('token');
  }

  public isAuthenticated(): boolean {
    // get the token
    var token: any = this.getToken();
    var tokenObj: any = {
      exp: 0
    };

    if(!token){ return false; }
    try {
      tokenObj = decode(token);
    } catch (e) {
      localStorage.clear();
      return false;
    }

    if (Date.now() / 1000 > tokenObj.exp) {
      return false;
    }
    return true;
  }

  login(email, password) {


    if( localStorage.getItem('token') ){
      localStorage.removeItem('token');
    }

    return this.http.post(`${environment.site_url}/api/login`, {
      email: email,
      password: password
    }).pipe(map((res: any) => {

      var res = res.json();

      if( res.user && res.token ){
        localStorage.setItem('token', res.token);
      }

      return res;
    }));

  }

}
