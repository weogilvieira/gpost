import { Component, OnInit } from '@angular/core';
import { Router, Event, NavigationStart, NavigationEnd } from '@angular/router';
import { SiteService } from '../service/site.service';
import { PostTypeService } from '../service/posttype.service';
import { environment } from '../../environments/environment';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  menuActive: boolean = true;
  siteConfig: any = {};
  loaded: boolean = false;
  SITE_URL = environment.site_url;
  is_production = environment.production;
  postTypes = [];

  constructor(
    private router: Router,
    private siteService: SiteService,
    private titleService: Title,
    private postTypeService: PostTypeService
    ) {
      this.siteService.getConfig()
        .subscribe(res => {
          this.siteConfig = res.data;
          this.loaded = true;
          this.titleService.setTitle(`${this.getConfigByKey('sitename')}`);
        });

      this.postTypeService.getAll()
        .subscribe(res => {
          this.postTypes = res.data || [];
        })

      this.router.events.subscribe( (event: Event) => {
        this.menuActive = false;
      })
  }

  ngOnInit() {
  }

  getConfigByKey(key) {
    let found = this.siteConfig.keys.find((a) => {
      return a.key_slug == key;
    });
    return found.key_value;
  };

  logout(){
    var confirm = window.confirm("Deseja fazer logout?");
    if(!confirm){ return; }

    localStorage.clear();
    this.router.navigateByUrl("login")
  }

}
