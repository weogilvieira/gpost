import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './auth/token.interceptor';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

//LOCALE_ID
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import localePtExtra from '@angular/common/locales/extra/pt';
registerLocaleData(localePt, 'pt', localePtExtra);

//third-party modules
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ToastrModule } from 'ngx-toastr';
import { LocationPickerModule } from "ng-location-picker";

//services
import { Helper } from '../helper';
import { AuthGuard } from './auth/auth.guard';
import { AuthService } from './auth/auth.service';
import { MeService } from './service/me.service';
import { PagesService } from './service/pages.service';
import { PostService } from './service/post.service';
import { SiteService } from './service/site.service';
import { UserService } from './service/user.service';
import { FormsService } from './service/forms.service';
import { RelationshipService } from './service/relationship.service';
import { PostTypeService } from './service/posttype.service';

//components
import { AppComponent } from './app.component';
import { PostsComponent } from './page/posts/posts.component';
import { PagesComponent } from './page/pages/pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { MeComponent } from './page/me/me.component';
import { PageEditComponent } from './page/page-edit/page-edit.component';
import { UsersComponent } from './admin/users/users.component';
import { UsersEditComponent } from './admin/users-edit/users-edit.component';
import { NotfoundComponent } from './page/notfound/notfound.component';
import { BannersComponent } from './page/banners/banners.component';
import { BannersEditComponent } from './page/banners-edit/banners-edit.component';
import { ContactsComponent } from './page/contacts/contacts.component';
import { NewsletterComponent } from './page/newsletter/newsletter.component';
import { SettingsComponent } from './page/settings/settings.component';
import { RedirectsComponent } from './page/redirects/redirects.component';
import { MediaComponent } from './modal/media/media.component';
import { SettingsEditComponent } from './page/settings-edit/settings-edit.component';
import { FormsComponent } from './page/forms/forms.component';
import { CustomComponent } from './page/custom/custom.component';
import { CustomEditComponent } from './page/custom-edit/custom-edit.component';
import { TaxonomyComponent } from './modal/taxonomy/taxonomy.component';
import { GalleryComponent } from './page/gallery/gallery.component';
import { EditorComponent } from './editor/editor.component';
import { HeadComponent } from './components/head/head.component';
import { PosttypeComponent } from './page/posttype/posttype.component';
import { TablecontentComponent } from './components/tablecontent/tablecontent.component';
import { SearchbarComponent } from './components/searchbar/searchbar.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { PosttypeCatComponent } from './page/posttype-cat/posttype-cat.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: '',
    component: DashboardComponent,
    canActivate: [
      AuthGuard
    ],
    children: [
      {
          path:'',
          redirectTo: 'me',
          pathMatch: 'full'
      },
      {
        path: 'me',
        component: MeComponent,
        pathMatch: 'full'
      },
      {
        path: 'pages',
        component: PagesComponent,
        pathMatch: 'full'
      },
      {
        path: 'pages/new',
        component: PageEditComponent,
        pathMatch: 'full',
        data: {
          mode: 'new'
        }
      },
      {
        path: 'pages/edit/:id',
        component: PageEditComponent,
        pathMatch: 'full',
        data: {
          mode: 'edit'
        }
      },
      {
        path: 'posttype/:id',
        component: PosttypeComponent,
        pathMatch: 'full'
      },
      {
        path: 'posttype/:post_type/post',
        component: PostsComponent,
        pathMatch: 'full',
        data: {
          mode: 'new'
        }
      },
      {
        path: 'posttype/:post_type/post/:id',
        component: PostsComponent,
        pathMatch: 'full',
        data: {
          mode: 'edit'
        }
      },
      {
        path: 'posttype/:id/categories',
        component: PosttypeCatComponent,
        pathMatch: 'full'
      },
      {
        path: "users",
        component: UsersComponent,
        pathMatch: 'full'
      },
      {
        path: "users/new",
        component: UsersEditComponent,
        pathMatch: 'full',
        data: {
          mode: 'new'
        }
      },
      {
        path: "users/edit/:id",
        component: UsersEditComponent,
        pathMatch: 'full',
        data: {
          mode: 'edit'
        }
      },
      {
        path: "banners",
        component: BannersComponent,
        pathMatch: 'full'
      },
      {
        path: "banners/new",
        component: BannersEditComponent,
        pathMatch: 'full',
        data: {
          mode: 'new'
        }
      },
      {
        path: "banners/edit/:id",
        component: BannersEditComponent,
        pathMatch: 'full',
        data: {
          mode: 'edit'
        }
      },
      {
        path: "gallery",
        component: GalleryComponent,
        pathMatch: 'full'
      },
      {
        path: "gallery/new",
        component: BannersEditComponent,
        pathMatch: 'full',
        data: {
          mode: 'new'
        }
      },
      {
        path: "gallery/edit/:id",
        component: BannersEditComponent,
        pathMatch: 'full',
        data: {
          mode: 'edit'
        }
      },
      {
        path: "contacts",
        component: ContactsComponent,
        pathMatch: 'full'
      },
      {
        path: "newsletter",
        component: NewsletterComponent,
        pathMatch: 'full'
      },
      {
        path: "settings",
        component: SettingsComponent,
        pathMatch: 'full'
      },
      {
        path: "settings/new",
        component: SettingsEditComponent,
        pathMatch: 'full',
        data: {
          mode: 'new'
        }
      },
      {
        path: "settings/edit/:id",
        component: SettingsEditComponent,
        pathMatch: 'full',
        data: {
          mode: 'edit'
        }
      },
      {
        path: "custom/:slug",
        component: CustomComponent,
        pathMatch: 'full'
      },
      {
        path: "custom/:slug/new",
        component: CustomEditComponent,
        pathMatch: 'full',
        data: {
          mode: 'new'
        }
      },
      {
        path: "custom/:slug/edit/:id",
        component: CustomEditComponent,
        pathMatch: 'full',
        data: {
          mode: 'edit'
        }
      },
      {
        path: "redirects",
        component: RedirectsComponent,
        pathMatch: 'full'
      },
      {
        path: "forms/:slug",
        component: FormsComponent,
        pathMatch: 'full'
      },
      { path: '404', component: NotfoundComponent },
      { path: '**', redirectTo: '404' }
    ],
  }
];

@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    PagesComponent,
    DashboardComponent,
    LoginComponent,
    MeComponent,
    PageEditComponent,
    UsersComponent,
    UsersEditComponent,
    NotfoundComponent,
    BannersComponent,
    BannersEditComponent,
    ContactsComponent,
    NewsletterComponent,
    SettingsComponent,
    RedirectsComponent,
    MediaComponent,
    SettingsEditComponent,
    FormsComponent,
    CustomComponent,
    CustomEditComponent,
    TaxonomyComponent,
    GalleryComponent,
    EditorComponent,
    HeadComponent,
    PosttypeComponent,
    TablecontentComponent,
    SearchbarComponent,
    PaginationComponent,
    PosttypeCatComponent
  ],
  imports: [
      RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false,
        onSameUrlNavigation: 'reload'
      }
    ),
    LocationPickerModule,
    BrowserModule,
    NgbModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CKEditorModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: LOCALE_ID,
      useValue: 'pt'
    },
    Helper,
    AuthGuard,
    AuthService,
    PagesService,
    MeService,
    SiteService,
    PostService,
    UserService,
    RelationshipService,
    PostTypeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
