import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { SiteService } from '../service/site.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Helper } from '../../helper';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)])
  });

  editor: boolean = true;
  siteConfig: any = {};
  loaded: boolean = false;
  SITE_URL: string = environment.site_url;

  constructor(
    private siteService: SiteService,
    public authService: AuthService,
    private router: Router,
    private helper: Helper
  ) {

    if( this.authService.isAuthenticated() ) {
      this.router.navigateByUrl("/");
    }

    this.siteService.getConfig()
      .subscribe(res => {
        this.siteConfig = res.data;
        this.loaded = true;
      });

  }

  ngOnInit() {
  }


  loginUser(form) {

    if(!form.valid ){ return; }
    this.editor = false;
    let _self = this;

    var obj = form.value;

    this.authService.login(obj.email, obj.password)
      .subscribe( res => {
        if( this.authService.isAuthenticated() ){
          this.router.navigateByUrl("/")
        }
        else if( res.message ) {
          alert(res.message);
        }

        _self.editor = true;

      }, err => {
        _self.editor = true;
      });
  }

}
