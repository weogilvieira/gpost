import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { MeService } from '../../service/me.service';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-me',
  templateUrl: './me.component.html',
  styleUrls: ['./me.component.scss']
})
export class MeComponent implements OnInit {

  editor: boolean = false;
  editorPass: boolean = false;
  user: any = {};
  formData = new FormGroup({
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    front_role: new FormControl('', []),
    bio: new FormControl('', Validators.maxLength(150))
  });

  passData = new FormGroup({
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    passwordB: new FormControl('', [Validators.required, Validators.minLength(6)])
  });

  constructor(
    private meService: MeService,
    private router: Router,
    private titleService: Title
  ) {
    this.titleService.setTitle("Minha Conta")
  }


  ngOnInit() {
    this.editor = false;
    this.editorPass = false;

    this.meService.get()
      .subscribe( res => {
        this.user = res.data;

        this.formData.patchValue({
          name: this.user.name,
          email: this.user.email,
          front_role: this.user.front_role,
          bio: this.user.bio
        });

        this.editor = true;
        this.editorPass = true;
      });
  }

  update(form) {

    if(!form.valid ){ return; }

    this.editor = false;

    var obj = form.value;

    this.meService.put(obj)
    .subscribe( res => {

      if( res.message ){
        alert(res.message);
      }

      this.editor = true;
    }, error => {
      alert("Erro ao tentar atualizar, tente novamente.");
      this.editor = true;
    });

    return;
  }

  updatePass(form) {

    if(!form.valid ){
      alert("Preencha os campos de Senha corretamente.");
      return;
    }

    this.editor = false;

    var obj = form.value;

    if( obj.password != obj.passwordB ){
      alert("As senhas são diferentes.");
      return;
    }

    this.editorPass = false;

    this.meService.updatePass(obj.password)
    .subscribe( res => {

      alert(res.message);

      this.passData.patchValue({
        password: '',
        passwordB: '',
      });

      this.editorPass = true;

    }, error => {
      alert("Erro ao atualizar, tente novamente.");
      this.editorPass = true;
    });

    return;
  }


}
