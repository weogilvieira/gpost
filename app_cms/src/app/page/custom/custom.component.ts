import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Title } from '@angular/platform-browser';
import { CustomService } from '../../service/custom.service';
import { SiteService } from '../../service/site.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators } from '@angular/forms';


@Component({
  selector: 'app-custom',
  templateUrl: './custom.component.html',
  styleUrls: ['./custom.component.scss']
})
export class CustomComponent implements OnInit {

  AREAS: any = [];
  AREA_KEY: string = null;
  PAGE: number = 1;

  data: any = {};
  areaConfig: any = {};
  loaded: boolean = false;
  query = new FormControl('');

  constructor(
    private siteService: SiteService,
    private customService: CustomService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title
    ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {

    this.AREA_KEY = this.activatedRoute.snapshot.paramMap.get('slug');

    this.siteService.getConfig()
      .subscribe( res => {
        this.AREAS = res.data.custom_areas || [];
        var areaConfig = this.AREAS.filter( (a, index) => {
          return (a.key == this.AREA_KEY)
        });

        this.areaConfig = areaConfig.length ? areaConfig[0] : null;
        this.titleService.setTitle(`${this.areaConfig.title}`);


        this.load();

      })
  }


  load(page = null) {
    this.loaded = false;
    this.PAGE = page || this.PAGE;

    let obj = {};

    if(this.query.value) {
      obj['query'] = this.query.value;
    }

    this.customService.getAll(obj, this.AREA_KEY, this.PAGE)
      .subscribe( res => {
        this.data = res.data;
        this.loaded = true;
        console.log(this.data, this.loaded)
      });
  }

  resetSearch() {
    if(!this.query.value){ return; }
    this.query.setValue('');
    this.load(1);
  }

}
