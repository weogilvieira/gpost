import { Component, OnInit, ElementRef, ViewChild, Injector  } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators } from '@angular/forms';
import { Helper } from '../../../helper';
import { PostService } from '../../service/post.service';
import { SiteService } from '../../service/site.service';
import { MediaService } from '../../service/media.service';
import { TaxonomyService } from '../../service/taxonomy.service';
import { RelationshipService } from '../../service/relationship.service';
import { PostTypeService } from '../../service/posttype.service';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  SITE_URL = environment.site_url;
  HOURS: any = new Array(24);
  MINUTES: any = new Array(60);
  DATE: any = new Date();

  POST: any = null;
  MODE: string = "new";
  ID: string = "";
  EDIT_SLUG: boolean = false;
  CATEGORIES: any = [];
  TAGS: any = [];
  TAGS_AVAILABLE: any = [];
  OBJ_TAGS: any = [];
  loaded: boolean = false;
  siteConfig: any = {};
  postType: any = {};
  taxonomyMode: string = "category";

  SHOW_MODAL_MEDIA: boolean = false;
  SHOW_MODAL_TAXONOMY: boolean = false;
  OBJ_COVER: any = null;
  PUBLISHED_AT = { "year": this.DATE.getFullYear(), "month": this.DATE.getMonth()+1, "day": this.DATE.getDate() }

  timeOutPreSave: any = null;


  form = new FormGroup({
    title: new FormControl('', [Validators.required]),
    slug: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    content: new FormControl('<div></div>'),
    seo_title: new FormControl(''),
    seo_description: new FormControl(''),
    cover: new FormControl(),
    category: new FormControl('0', [Validators.required]),
    tags: new FormControl('0'),
    status: new FormControl(1),
    highlighted: new FormControl(0),
    published_at_hour: new FormControl(this.DATE.getHours()),
    published_at_minutes: new FormControl(this.DATE.getMinutes())
  });

  editor: boolean = false;


  constructor(
    private postTypeService: PostTypeService,
    private postService: PostService,
    private siteService: SiteService,
    private mediaService: MediaService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private helper: Helper,
    private taxonomyService: TaxonomyService,
    private relationshipService: RelationshipService
    ) {
  }

  ngOnInit() {

    let postTypeID = Number(this.activatedRoute.snapshot.paramMap.get('post_type') ? this.activatedRoute.snapshot.paramMap.get('post_type') : 1);

    this.activatedRoute.data.subscribe((data) => {
      if( data.mode ){ this.MODE = data.mode; }
    });

    this.postTypeService.get(postTypeID)
      .subscribe(res => {
        this.postType = res.data;

        this.loadTaxs();

        if( this.MODE == "edit" ){
          this.ID = this.activatedRoute.snapshot.paramMap.get('id');

          this.postService.get(this.ID)
            .subscribe( res => {
              this.POST = res.data;
              this.populate();
            });

          this.loadPostTags();

          this.loaded = true;
        } else {
          this.loaded = true;
        }


      });
  }

  editCover() { this.SHOW_MODAL_MEDIA = true; }

  removeTagFromPost(item) {

    var itemInfo = this.getTagInfo((item.taxonomy_id || item.id ));

    let confirm = window.confirm(`Deseja remover a tag "${itemInfo ? itemInfo.title : 'desconhecida'}"?`)

    if(!confirm){ return; }


    if( this.ID ){
    this.relationshipService.delete(item.id)
      .subscribe( res => {

        this.OBJ_TAGS = this.OBJ_TAGS.filter(a => {
          return (a.id != item.id);
        });

        this.setTagsAvailable();
      });
    } else {

      this.OBJ_TAGS = this.OBJ_TAGS.filter(a => {
        return (a.id != item.id);
      });

      this.setTagsAvailable();

    }

    this.setTagsAvailable();

  }

  getObjTags() {
    let _self = this;

    return this.OBJ_TAGS.map(a => {
      return _self.getTagInfo((a.data.taxonomy_id || a.data.id));
    });

    this.setTagsAvailable();
  };

  getTagInfo(id){

    let r = this.TAGS.find(a => {
      return (a.data.id == Number(id));
    });

    if( !r ){ return; }
    return r.data;
  }

  setTagsAvailable(){
    if(!this.TAGS.length){ return; }

    let IDS_UNAVAILABLE = [];

    this.OBJ_TAGS = this.OBJ_TAGS.filter(a => {

      let r = this.TAGS.find(b => {
        return (b.data.id.toString() == a.taxonomy_id || b.data.id.toString() == a.id);
      });

      if( !!r ){ IDS_UNAVAILABLE.push(Number(a.taxonomy_id || a.id)) };
      return !!r;
    });

    this.TAGS_AVAILABLE = this.TAGS.filter(a => {
      return (IDS_UNAVAILABLE.indexOf(a.data.id) == -1);
    }).sort((a, b) => {
      return (a.data.title > b.title) ? 1 : -1;
    });

  }

  addTagToPost(id) {

    let tagId = ""+(id || this.form.value.tags);
    let object_id = this.ID;

    let r = this.OBJ_TAGS.find(a => {
      return (a.taxonomy_id == tagId || a.id == tagId);
    });

    if(!!r){ return; }

    if( !object_id ){

      var e = this.TAGS.find(a => {
        return a.data.id.toString() == tagId;
      });

      if(!!e){
        this.OBJ_TAGS.push(e.data);

        this.form.patchValue({
          tags: '0'
        });

        this.setTagsAvailable();

      };
      return;
    };

    this.relationshipService.post(object_id, {
      taxonomy_id : tagId
    })
      .subscribe( res => {
        if(!res.data && res.message){ return alert(typeof res.message == 'string' ? res.message : 'Algo errado. Tente novamente ou entre em contato com o suporte.'); }

        this.OBJ_TAGS = this.OBJ_TAGS.concat(res.data);

        this.setTagsAvailable();

        this.form.patchValue({
          tags: '0'
        });

        return;
      });

  }

  addTaxonomy(mode = "category") {
    this.taxonomyMode = mode;
    this.SHOW_MODAL_TAXONOMY = true;
  }

  populate() {

    this.POST.published_date = new Date(this.POST.published_date);

    this.POST.published_at_hour = this.POST.published_date.getHours();
    this.POST.published_at_minutes = this.POST.published_date.getMinutes();

    this.PUBLISHED_AT = { "year": this.POST.published_date.getFullYear(), "month": this.POST.published_date.getMonth()+1, "day": this.POST.published_date.getDate() }

    this.form.patchValue(this.POST);

    this.loadCover();
    this.editor = false;
  }

  removeCover() {
    this.OBJ_COVER = null;

    this.form.patchValue({
      cover: null
    });
  }

  loadCover() {
    if(!this.form.value.cover) return;

    this.mediaService.get(this.form.value.cover)
      .subscribe( res => {
        this.OBJ_COVER = res.data;
      });

  }

  generateSlug(text = null) {

    if( this.EDIT_SLUG && !this.form.value.slug ){ return; }

    if( !text && this.form.value.slug){ return; }
    this.form.patchValue({
      slug: this.helper.slugify(text || this.form.value.title)
    });
  }

  saveSlug(){
    this.EDIT_SLUG = false;
    if(!this.form.value.slug){
      this.generateSlug(this.form.value.title);
    }
  }

  getImage( obj ){
    console.log(obj);
    this.form.patchValue({
      cover: obj.id
    });

    this.OBJ_COVER = obj;

    // this.loadCover();
  }

  setContent( obj ) {

    this.form.patchValue({
      content: obj
    });
  }

  setTax( obj ){

    if( obj.type == 'category' ){
      this.form.patchValue({
        category: obj.id
      });
    }

    if( obj.type == 'tag' ){
      this.addTagToPost(obj.id)
    }

    this.loadTaxs();
  }

  closeModalTax(){
    this.SHOW_MODAL_TAXONOMY = false;
    this.loadTaxs();
  }

  getCategorySlug(id) {
    let arr = this.CATEGORIES.filter(a => {
      return id == a.data.id;
    });

    let r = arr.length > 0 ? arr[0].data.slug : "";

    return r;
  }

  loadTaxs() {
    this.taxonomyService.getAll(this.postType.id, 'category')
      .subscribe( res => {
        this.CATEGORIES = res.data;
      });

    this.taxonomyService.getAll(this.postType.id, 'tag')
      .subscribe( res => {
        this.TAGS = res.data;
        this.setTagsAvailable();
      });


  }

  loadPostTags() {
    if(!this.ID){ return; }

    this.relationshipService.getAll(this.ID)
      .subscribe( res => {
        this.OBJ_TAGS = res.data;

        this.setTagsAvailable();
      });
  }

  save(form, exit: boolean = false) {

    if( this.MODE == 'new' ){ clearTimeout(this.timeOutPreSave) }

    if(!form.valid){ alert("Alguns campos são obrigatórios."); }

    let obj = form.value;
    obj.post_type = this.postType.id;

    let published_date = new Date(this.PUBLISHED_AT.year, this.PUBLISHED_AT.month-1, this.PUBLISHED_AT.day, obj.published_at_hour, obj.published_at_minutes, 0, 0);
    obj.published_date = published_date;

    if( obj.category == "0" ){
      alert("Selecione a categoria.");
      return;
    }

    if(!this.ID){
      var arr_tags = [];
      this.OBJ_TAGS.map(a => {
        arr_tags.push(a.id);
      });
      obj.tags = arr_tags.join(",");
    }

    this.postService[(this.MODE == 'edit') ? 'put' : 'post'](obj, this.ID)
      .subscribe(res => {

        if( res.message && !res.data ){
          alert(typeof res.message == 'string' ? res.message : 'Algo errado. Tente novamente ou entre em contato com o suporte.');
          console.error(res.message);
          return;
        }

        if( res.data ){
          return exit ? this.router.navigateByUrl(`/posttype/${this.postType.id}`) : this.router.navigateByUrl(`/posttype/${this.postType.id}/edit/${ res.data.id }`);
        }

      }, error => {
        let msg = "Erro ao "+(this.MODE != 'edit' ? 'registrar' : 'atualizar')+", tente novamente.";
        alert(error.message ? error.message : msg);
        this.editor = true;
      });
  };


  delete() {
    var confirm = window.confirm("Deseja mesmo deletar este item?");


    if(confirm){

      return this.postService.delete(this.ID)
        .subscribe( res => {
          if( res.message ){
            alert(typeof res.message == 'string' ? res.message : 'Algo errado. Tente novamente ou entre em contato com o suporte.');
          }

          if( res.data ){
            alert("Item deletado com sucesso.");
            return this.router.navigateByUrl(`/posttype/${this.postType.id}`);
          }
        })

    }
  }

  preSaveNewPost() {
    if(!this.form.valid || this.form.value.category == "0" ){ return; }

    this.form.patchValue({ status: 0 });
    this.save(this.form);
  }


  cancel() {
    var confirm = window.confirm("Deseja sair sem salvar?");
    if(confirm){
      return this.router.navigateByUrl(`/posttype/${this.postType.id}`);
    }
  }

}
