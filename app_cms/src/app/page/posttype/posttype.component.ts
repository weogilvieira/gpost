import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PostTypeService } from '../../service/posttype.service';
import { PostService } from '../../service/post.service';
import { TaxonomyService } from '../../service/taxonomy.service';

@Component({
  selector: 'app-posttype',
  templateUrl: './posttype.component.html',
  styleUrls: ['./posttype.component.scss']
})

export class PosttypeComponent implements OnInit {
  ID: number = 1;
  PAGE: number = 1;
  data: any = null;
  query: string = "";
  categories: any = [];
  posts: any = [];
  columns: any = [];
  loaded: boolean = false;
  searchFilters: any = [];
  queryForm: any = {};

  constructor(
    private postTypeService: PostTypeService,
    private postService: PostService,
    private activatedRoute: ActivatedRoute,
    private taxonomyService: TaxonomyService,
    private router: Router
    ) {

    this.ID = Number(this.activatedRoute.snapshot.paramMap.get('id') ? this.activatedRoute.snapshot.paramMap.get('id') : 1);

    this.postTypeService.get(this.ID)
      .subscribe(res => {
        this.data = res.data;
        this.loadPosts();
      });

  }

  ngOnInit() {

    this.columns = [
      {
        title: "Titulo",
        data_type: "link",
        getData: (item) => {
          return {
            path: `/posttype/${this.ID}/post/${item.data.id}`,
            label: item.data.title
          }
        }
      },
      {
        title: "Categoria",
        getData: (item) => {
          return (item.data.category && item.data.category.data ? item.data.category.data.title : '---');
        }
      },
      {
        title: "Data",
        data_type: 'date',
        getData: (item) => {
          return item.data.published_date;
        }
      },
      {
        title: "Status",
        data_type: 'boolean',
        getData: (item) => {
          return {
            value: item.data.status,
            label: (item.data.status ? 'Publicado' : 'Rascunho')
          };
        }
      }
    ];
  }

  buildFilters() {

    this.searchFilters = [
      {
        name : "status",
        placeholder : "Status",
        type: "select",
        default: "",
        value: [
          ":Status",
          "1:Publicado",
          "0:Rascunho",
        ],
        minlength: null,
        maxlength: null,
        required: false
      },
      {
        name : "category",
        placeholder : "Categoria",
        type: "select",
        default: "",
        value: (() => {
          let r = [":Categorias"];
          this.categories.forEach(a => {
            r.push(`${a.data.id}:${a.data.title}`);
          });
          return r;
        })(),
        minlength: "",
        maxlength: "",
        required: false
      }
    ];

  };

  newPost() {
    this.router.navigateByUrl(`/posttype/${this.ID}/post`);
  }

  submitSearch(form) {
    this.queryForm = form;
    this.query = form.query;
    this.loadPosts(1);
  }

  resetSearch(){
    this.queryForm = {};
    this.query = "";
    this.loadPosts(1);
  }

  getCat( id, attr){
    if(!this.categories.length){ return null; }
    var cat = this.categories.find(a => Number(a.data.id) == Number(id) );
    return cat ? cat.data[attr] : null;
  }

  loadPosts(page = null) {
    this.loaded = false;
    this.PAGE = page || this.PAGE;

    var obj = {
      page: this.PAGE
    };

    Object.keys(this.queryForm).map(a => {
      if(this.queryForm[a]){
        obj[a] = this.queryForm[a];
      }
    });

    this.postTypeService.getPosts(this.ID, obj)
      .subscribe( res => {
        this.posts = res.data;
        this.taxonomyService.getAll(this.ID)
          .subscribe( res => {
            this.categories = res.data;
            this.loaded = true;
            this.buildFilters();
          });
      });
  }

}
