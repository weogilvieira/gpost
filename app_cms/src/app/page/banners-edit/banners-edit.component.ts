import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-banners-edit',
  templateUrl: './banners-edit.component.html',
  styleUrls: ['./banners-edit.component.scss']
})
export class BannersEditComponent implements OnInit {
  @ViewChild('mediaModal', { static: false }) mediaModal: ElementRef;

  constructor() { }

  AfterViewInit() {
    console.log("mediaModal", this.mediaModal );
  }

  ngOnInit() {
  }

}
