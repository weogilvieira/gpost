import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Title } from '@angular/platform-browser';
import { FormsService } from '../../service/forms.service';
import { SiteService } from '../../service/site.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {
  FORMS: any = [];
  FORM_KEY: string = null;
  PAGE: number = 1;

  data: any = {};
  formConfig: any = {};
  loaded: boolean = false;
  query = new FormControl('');

  constructor(
    private siteService: SiteService,
    private formsService: FormsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title
    ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {

    this.FORM_KEY = this.activatedRoute.snapshot.paramMap.get('slug');

    this.siteService.getConfig()
      .subscribe( res => {
        this.FORMS = res.data.forms || [];
        var formConfig = this.FORMS.filter( (a, index) => {
          return (a.key == this.FORM_KEY)
        });

        this.formConfig = formConfig.length ? formConfig[0] : null;
        this.titleService.setTitle(`Formulário: ${this.formConfig.title}`);


        this.load();

      })
  }

  getKeys( obj ){
    return Object.keys(obj);
  }

  getCSV(){
    this.formsService.getCSV(this.FORM_KEY)
      .subscribe( res => {

        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.');
      });
  }


  load(page = null) {
    this.loaded = false;
    this.PAGE = page || this.PAGE;

    this.formsService.getAll(this.FORM_KEY, this.PAGE)
      .subscribe( res => {
        this.data = res.data;

        if(this.data.docs) {
          this.data.docs.map(a => {
            a.inputs = JSON.parse(a.inputs);
            return a;
          });
        }

        this.loaded = true;
      });
  }

  resetSearch() {
    if(!this.query.value){ return; }
    this.query.setValue('');
    this.load(1);
  }

}
