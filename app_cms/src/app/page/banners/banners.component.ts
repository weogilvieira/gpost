import { Component, OnInit } from '@angular/core';
import { Router, Event, NavigationStart, NavigationEnd } from '@angular/router';
import { SiteService } from '../../service/site.service';

@Component({
  selector: 'app-banners',
  templateUrl: './banners.component.html',
  styleUrls: ['./banners.component.scss']
})
export class BannersComponent implements OnInit {

  siteConfig: any = {};
  loaded: boolean = false;
  items: any = [];

  constructor(
    private router: Router,
    private siteService: SiteService
  ) {
      this.siteService.getConfig()
        .subscribe(res => {
          this.siteConfig = res.data;
          this.items = this.siteConfig.banners;
          this.loaded = true;
        });
  }

  ngOnInit() {
  }

}
