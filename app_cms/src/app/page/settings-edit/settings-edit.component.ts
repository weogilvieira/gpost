import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Title } from '@angular/platform-browser';
import { SiteService } from '../../service/site.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Helper } from '../../../helper';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-settings-edit',
  templateUrl: './settings-edit.component.html',
  styleUrls: ['./settings-edit.component.scss']
})
export class SettingsEditComponent implements OnInit {

  MODE: string = "new";
  ID: string = "";
  DATA: any = {};
  EDIT_SLUG: boolean = false;

  loaded: boolean = false;


  form = new FormGroup({
    key_name: new FormControl('', [Validators.required]),
    key_slug: new FormControl('', [Validators.required]),
    key_value: new FormControl('', [Validators.maxLength(255)]),
    key_type: new FormControl('TEXT', [Validators.required]),
    custom_type: new FormControl(1, [Validators.required])
  });


  constructor(
    private siteService: SiteService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private helper: Helper
  ) { }

  ngOnInit() {
    this.titleService.setTitle("Editar chave de Configuração");

    this.activatedRoute.data.subscribe((data) => {
      if( data.mode ){ this.MODE = data.mode; }
    });


    if( this.MODE == "edit") {
      this.ID = this.activatedRoute.snapshot.paramMap.get('id');
      this.siteService.get(this.ID)
        .subscribe( res => {
          this.DATA = res.data;
          this.form.patchValue(this.DATA);
          this.loaded = true;
        });
    } else {
      this.loaded = true;
    }
  }


  generateSlug(text = null) {

    if(!text && this.form.value.key_slug){ return; }

    this.form.patchValue({
      key_slug: this.helper.slugify(text || this.form.value.key_name)
    });
  }



  save(form, exit: boolean = false) {

    if(!form.valid){
      alert("Alguns campos são obrigatórios.");
      return;
    }

    var obj = form.value;

    this.siteService[(this.MODE == 'edit') ? 'put' : 'post'](obj, this.ID)
      .subscribe(res => {

        if( res.message ){
          alert(res.message);
        }

        if( res.data ){
          return (this.MODE == 'edit') ? this.router.navigateByUrl("/settings") : this.router.navigateByUrl(`/settings/edit/${ res.data.id }`);
        }

      }, error => {
        let msg = "Erro ao "+(this.MODE != 'edit' ? 'registrar' : 'atualizar')+", tente novamente.";
        alert(error.message ? error.message : msg);
      });
  };


  delete() {
    var confirm = window.confirm("Deseja mesmo deletar este item?");
    if(confirm){

      return this.siteService.delete(this.ID)
        .subscribe( res => {
          if( res.message ){
            alert(res.message);
          }

          if( res.data ){
            alert("Item deletado com sucesso.");
            return this.router.navigateByUrl("/settings");
          }
        })

    }
  }



  cancel(){
    var confirm = window.confirm("Deseja sair sem salvar?");
    if(confirm){
      return this.router.navigateByUrl("/settings");
    }
  }

}
