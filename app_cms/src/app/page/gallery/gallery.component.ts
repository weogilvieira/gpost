import { Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import { TaxonomyService } from '../../service/taxonomy.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  SITE_URL = environment.site_url;
  items: any = [];
  loaded: boolean = true;
  PAGE: any = 1;
  target = "gallery";
  type = "category";

  form = new FormGroup({
    title: new FormControl('', [Validators.required, Validators.minLength(3)])
  });


  constructor(
    private taxonomyService: TaxonomyService
  ) { }

  ngOnInit() {
    this.loadData()
  }


  create( form ) {
    if(!form.valid){ alert("Nome da galeria vázio ou muito curto."); }
    let obj = form.value;

    let _self = this;

    this.taxonomyService.post(this.target, this.type, obj)
      .subscribe( res => {
        if( res.message ){ alert(res.message); return; }
        _self.loadData(1);
        _self.form.reset();
      });
  }

  update( item ) {
    if(item.title.length <= 3){ alert("Nome da categoria vázio ou muito curto."); }
    let obj = item;
    let _self = this;

    this.taxonomyService.put(obj.id, obj)
      .subscribe( res => {
        if( res.message ){ alert(res.message); return; }
        _self.loadData();
        _self.form.reset();
      });
  }

  delete(id) {
    if(!id){ return; }
    var confirm = window.confirm("Deseja mesmo deletar este item?");
    if(!confirm){ return; }
    this.loaded = false;

    return this.taxonomyService.delete(id)
      .subscribe( res => {
        if( res.message ){ alert(res.message); }

        if( res.data ){
          alert("Item deletado com sucesso.");
          this.loadData();
        }
      })

  }

  loadData(page = null) {
    this.PAGE = page || this.PAGE;
    this.loaded = false;

    this.taxonomyService.getAll(this.target, this.type)
      .subscribe( res => {
        this.items = res.data;
        this.loaded = true;
      });
  }

}
