import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Title } from '@angular/platform-browser';
import { SiteService } from '../../service/site.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  data: any = {};
  loaded: boolean = false;

  constructor(
    private siteService: SiteService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title
    ) { }

  ngOnInit() {

    this.titleService.setTitle("Configurações do site");

    this.siteService.getConfig()
      .subscribe( res => {
        this.data = res.data;
        this.loaded = true;
      })


  }

}
