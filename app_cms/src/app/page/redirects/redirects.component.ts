import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-redirects',
  templateUrl: './redirects.component.html',
  styleUrls: ['./redirects.component.scss']
})
export class RedirectsComponent implements OnInit {

  ITEMS: any = [];

  constructor() { }

  ngOnInit() {
    this.add();
  }

  remove(index) {
    console.log(index);
    this.ITEMS.splice(index, 1);
    console.log(this.ITEMS);
  }


  add() {
    var fg = new FormGroup({
      from: new FormControl('', [Validators.required]),
      to: new FormControl('', [Validators.required]),
      type: new FormControl('302')
    });

    this.ITEMS.push(fg)
  }

}
