import { Component, OnInit, ElementRef, ViewChild, Injector  } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators } from '@angular/forms';
import { Helper } from '../../../helper';
import { PagesService } from '../../service/pages.service';

@Component({
  selector: 'app-page-edit',
  templateUrl: './page-edit.component.html',
  styleUrls: ['./page-edit.component.scss']
})

export class PageEditComponent implements OnInit {

  SITE_URL = environment.site_url;

  PAGE: any = null;
  PAGES: any = [];
  TEMPLATES: any = [];
  HOOKS: any = [];
  MODE: string = "new";
  ID: string = "";
  PARENT: any = "";
  EDIT_SLUG: boolean = false;
  editor: boolean = false;

  SHOW_MODAL_MEDIA: boolean = false;
  OBJ_COVER: any = null;

  form = new FormGroup({
    title: new FormControl('', [Validators.required]),
    slug: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    content: new FormControl(''),
    seo_title: new FormControl(''),
    seo_description: new FormControl(''),
    parent: new FormControl(''),
    template: new FormControl(''),
    page_hook: new FormControl(''),
    cover: new FormControl()
  });

  constructor(
    private pagesService: PagesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private helper: Helper
    ) {
  }

  ngOnInit() {

    this.activatedRoute.data.subscribe((data) => {
      if( data.mode ){ this.MODE = data.mode; }
    });

    this.titleService.setTitle(this.MODE == "new" ? "Nova Página" : "Editar Página");

    if( this.MODE == "edit") {
      this.ID = this.activatedRoute.snapshot.paramMap.get('id');

      this.pagesService.get(this.ID)
        .subscribe( res => {
          this.PAGE = res.data;
          this.form.patchValue(this.PAGE);

          this.setParent();
        });
    }

    this.pagesService.getAll()
      .subscribe( res => {
        this.PAGES = res.data.filter(a => {
          if(this.MODE == "edit"){
            return (a.id != this.ID);
          } else {
            return (!a.parent);
          }
        });

        this.setParent();
      });

    this.pagesService.getTemplates()
      .subscribe( res => {
        this.TEMPLATES = res.data;
      })

    this.pagesService.getHooks()
      .subscribe( res => {
        this.HOOKS = res.data;
      })

    // this.pagesService.get(this.ID)
  }

  editCover() { this.SHOW_MODAL_MEDIA = true; }

  removeCover() {
    this.OBJ_COVER = null;
    this.form.patchValue({
      cover: null
    });
  }

  generateSlug(text = null) {

    if( this.EDIT_SLUG && !this.form.value.slug ){ return; }

    if(!text && this.form.value.slug){ return; }
    this.form.patchValue({
      slug: this.helper.slugify(text || this.form.value.title)
    });
  }

  saveSlug(){
    this.EDIT_SLUG = false;
    if(!this.form.value.slug){
      this.generateSlug(this.form.value.title);
    }
  }

  setParent() {
    if( this.form.value.parent ){
      this.PAGES.map( a => {
        if( a.id == this.form.value.parent){
          this.PARENT = a;
        };
      });
    } else {
      this.PARENT = null;
    }
  }

  getImage( obj ){
    this.OBJ_COVER = obj;
    this.form.patchValue({
      cover: obj.id
    });
  }


  setContent( obj ) {
    this.form.patchValue({
      content: obj
    });
  }

  save(form, exit: boolean = false) {

    if(!form.valid){
      alert("Alguns campos são obrigatórios.");
      return;
    }

    var obj = form.value;

    this.pagesService[(this.MODE == 'edit') ? 'put' : 'post'](obj, this.ID)
      .subscribe(res => {

        if( res.message && !res.data ){
          (typeof res.message == "string") ? alert(res.message) : alert(res.message.errors.join("\n"));
          return;
        }

        if( res.data ){
          return exit ? this.router.navigateByUrl("/pages") : this.router.navigateByUrl(`/pages/edit/${ res.data.id }`);
        }

      }, error => {
        let msg = "Erro ao "+(this.MODE != 'edit' ? 'registrar' : 'atualizar')+", tente novamente.";
        alert(error.message ? error.message : msg);
        this.editor = true;
      });
  };

  delete() {
    var confirm = window.confirm("Deseja mesmo deletar este item?");
    if(confirm){

      return this.pagesService.delete(this.ID)
        .subscribe( res => {
          if( res.data && res.message ){
            alert(res.message);
            return;
          }

          if( res.data ){
            alert("Página deletada com sucesso.");
            return this.router.navigateByUrl("/pages");
          }
        })

    }
  }

  cancel(){
    var confirm = window.confirm("Deseja sair sem salvar?");
    if(confirm){
      return this.router.navigateByUrl("/pages");
    }
  }


}
