import { Component, OnInit, ElementRef, ViewChild, Injector  } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators } from '@angular/forms';
import { Helper } from '../../../helper';
import { CustomService } from '../../service/custom.service';
import { SiteService } from '../../service/site.service';
import { MediaService } from '../../service/media.service';
import * as ClassicEditor from 'ckeditor5-build-gpost';


@Component({
  selector: 'app-custom-edit',
  templateUrl: './custom-edit.component.html',
  styleUrls: ['./custom-edit.component.scss']
})
export class CustomEditComponent implements OnInit {
  public CKEditor = ClassicEditor;
  SITE_URL = environment.site_url;
  HOURS: any = new Array(24);
  MINUTES: any = new Array(60);
  DATE: any = new Date();

  AREAS: any = [];
  AREA_KEY: string = null;
  areaConfig: any = {};
  loaded: boolean = false;
  siteConfig: any = {};
  MODE: string = "new";
  ID: string = "";
  CUSTOM: any = {};

  form = new FormGroup({
    title: new FormControl('', [Validators.required]),
    status: new FormControl(1),
    published_at_hour: new FormControl(this.DATE.getHours()),
    published_at_minutes: new FormControl(this.DATE.getMinutes()),
  });

  inputs = new FormGroup({});

  PUBLISHED_AT = { "year": this.DATE.getFullYear(), "month": this.DATE.getMonth()+1, "day": this.DATE.getDate() }



  constructor(
    private customService: CustomService,
    private siteService: SiteService,
    private mediaService: MediaService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private helper: Helper
    ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }


  ngOnInit() {

    this.activatedRoute.data.subscribe((data) => {
      if( data.mode ){ this.MODE = data.mode; }
    });

    this.AREA_KEY = this.activatedRoute.snapshot.paramMap.get('slug');

    this.siteService.getConfig()
      .subscribe( res => {
        this.AREAS = res.data.custom_areas || [];
        var areaConfig = this.AREAS.filter( (a, index) => {
          return (a.key == this.AREA_KEY)
        });

        if(!areaConfig){
          alert("areaConfig não existe.");
          this.router.navigateByUrl("/");
          return;
        }

        this.areaConfig = areaConfig.length ? areaConfig[0] : null;
        this.titleService.setTitle(this.MODE == "new" ? `Novo item de ${this.areaConfig.title}` : `Editar item de ${this.areaConfig.title}`);

        this.buildForm();

        if( this.MODE == "edit" ){
          this.ID = this.activatedRoute.snapshot.paramMap.get('id');

          this.customService.get(this.ID)
          .subscribe( res => {
            this.CUSTOM = res.data;
            this.populate();
          });

          this.loaded = true;
        } else {
          this.loaded = true;
        }

    });
  } //ngOnInit


  buildForm() {

    this.areaConfig.inputs.map(a => {
      let default_val = a.value || "";
      let validators = [];

      if(a.required){ validators.push(Validators.required); }
      if(a.minlength){ validators.push(Validators.minLength(a.minlength)); }
      if(a.maxlength){ validators.push(Validators.maxLength(a.maxlength)); }

      let ctrl = new FormControl(default_val, validators);

      this.inputs.addControl(a.key, ctrl);
    });

  }//buildForm

  populate() {

    this.CUSTOM.published_date = new Date(this.CUSTOM.published_date);

    this.CUSTOM.published_at_hour = this.CUSTOM.published_date.getHours();
    this.CUSTOM.published_at_minutes = this.CUSTOM.published_date.getMinutes();

    this.PUBLISHED_AT = { "year": this.CUSTOM.published_date.getFullYear(), "month": this.CUSTOM.published_date.getMonth()+1, "day": this.CUSTOM.published_date.getDate() }

    this.form.patchValue(this.CUSTOM);

    let inputs = this.CUSTOM.inputs;
    this.inputs.patchValue(this.CUSTOM.inputs);

    // this.loadCover();
  }//populate


  logPickedLocation($event, input) {
    let obj = {};
    obj[input.key] = $event;
    this.inputs.patchValue(obj);
  }

  delete() {
    var confirm = window.confirm("Deseja mesmo deletar este item?");
    if(confirm){

      return this.customService.delete(this.ID)
        .subscribe( res => {

          if( res.message && !res.data){
            alert(res.message);
            return;
          }

          if( res.data ){
            alert("Item deletado com sucesso.");
            return this.router.navigateByUrl(`/custom/${this.areaConfig.key}`);
          }
        })

    }
  }


  save(form, exit: boolean = false) {

    if(!form.valid || !this.inputs.valid){
      alert("Alguns campos são obrigatórios.");
      return;
    }

    let obj = form.value;
    let published_date = new Date(this.PUBLISHED_AT.year, this.PUBLISHED_AT.month-1, this.PUBLISHED_AT.day, obj.published_at_hour, obj.published_at_minutes, 0, 0);
    obj.published_date = published_date;
    obj.inputs = this.inputs.value;
    obj.key = this.AREA_KEY;

    this.customService[(this.MODE == 'edit') ? 'put' : 'post'](obj, this.ID)
      .subscribe(res => {

        if( res.message ){
          alert(res.message);
        }

        if( res.data ){
          return (exit || this.MODE == 'new') ? this.router.navigateByUrl(`/custom/${this.areaConfig.key}`) : this.router.navigateByUrl(`/custom/${this.areaConfig.key}/edit/${ res.data.id }`);
        }

      }, error => {
        let msg = "Erro ao "+(this.MODE != 'edit' ? 'registrar' : 'atualizar')+", tente novamente.";
        alert(error.message ? error.message : msg);
      });
  };

  cancel(){
    var confirm = window.confirm("Deseja sair sem salvar?");
    if(confirm){
      return this.router.navigateByUrl(`/custom/${this.areaConfig.key}`);
    }
  }
}
