import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosttypeCatComponent } from './posttype-cat.component';

describe('PosttypeCatComponent', () => {
  let component: PosttypeCatComponent;
  let fixture: ComponentFixture<PosttypeCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosttypeCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosttypeCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
