import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostTypeService } from '../../service/posttype.service';
import { PostService } from '../../service/post.service';
import { TaxonomyService } from '../../service/taxonomy.service';

@Component({
  selector: 'app-posttype-cat',
  templateUrl: './posttype-cat.component.html',
  styleUrls: ['./posttype-cat.component.scss']
})
export class PosttypeCatComponent implements OnInit {
  ID: string = "1";
  PAGE: number = 1;
  data: any = null;
  query: string = "";
  categories: any = [];
  posts: any = [];
  columns: any = [];
  loaded: boolean = false;
  searchFilters: any = [];
  queryForm: any = {};

  constructor(
    private postTypeService: PostTypeService,
    private postService: PostService,
    private taxonomyService: TaxonomyService,
    private router: Router
    ) {

    this.postTypeService.get(this.ID)
      .subscribe(res => {
        this.data = res.data;
        this.loadCats();
      });

  }

  ngOnInit() {

    this.columns = [
      {
        title: "#",
        getData: (item) => {
          return item.data.id
        }
      },
      {
        title: "Titulo",
        data_type: "link",
        getData: (item) => {
          return {
            path: `/posttype/${this.ID}/categories/${item.data.id}`,
            label: item.data.title
          }
        }
      },
      {
        title: "Slug",
        getData: (item) => {
          return item.data.slug;
        }
      },
      {
        title: "Tipo",
        getData: (item) => {
          return item.data.type;
        }
      }
    ];
  }

  buildFilters() {

    this.searchFilters = [
      {
        name : "status",
        placeholder : "Status",
        type: "select",
        default: "",
        value: [
          ":Status",
          "1:Publicado",
          "0:Rascunho",
        ],
        minlength: null,
        maxlength: null,
        required: false
      },
      {
        name : "category",
        placeholder : "Categoria",
        type: "select",
        default: "",
        value: (() => {
          let r = [":Categorias"];
          this.categories.forEach(a => {
            r.push(`${a.data.id}:${a.data.title}`);
          });
          return r;
        })(),
        minlength: "",
        maxlength: "",
        required: false
      }
    ];

  };

  newPost() {
    this.router.navigateByUrl(`/posttype/${this.ID}/post`);
  }

  submitSearch(form) {
    this.queryForm = form;
    this.query = form.query;

    console.log(this.queryForm);
    this.loadCats(1);
  }

  resetSearch(){
    this.queryForm = {};
    this.query = "";
    this.loadCats(1);
  }

  loadCats(page = null) {
    this.loaded = false;
    this.PAGE = page || this.PAGE;

    var obj = {
      page: this.PAGE
    };

    Object.keys(this.queryForm).map(a => {
      if(this.queryForm[a]){
        obj[a] = this.queryForm[a];
      }
    });

    this.taxonomyService.getAll(this.ID, 'category')
      .subscribe( res => {
        this.categories = res.data;
        this.loaded = true;
        this.buildFilters();
      });
  }

}
