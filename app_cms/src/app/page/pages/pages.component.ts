import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { PagesService } from '../../service/pages.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {

  data: any = {};
  pages: any = [];
  pagesChild: any = [];
  loaded: boolean = false;


  constructor(
    private pagesService: PagesService,
    private titleService: Title
  ) {
    this.titleService.setTitle("Páginas");
  }


  ngOnInit() {


    this.pagesService.getAll()
      .subscribe( res => {
        this.data = res.data;

        this.pages = res.data.filter(a => {
          return !a.parent;
        });

        this.pagesChild = res.data.filter(a => {
          return !!a.parent;
        });

        this.loaded = true;

      });
  }

}
