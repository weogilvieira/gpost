import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PagesService {

  constructor(
    private http: HttpClient
  ) { }

  getAll(){
    return this.http.get(`${environment.site_url}/api/pages`)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  get( id ){
    var id = id || null;
    return this.http.get(`${environment.site_url}/api/pages/${id}`)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  getTemplates(){
    return this.http.get(`${environment.site_url}/api/pages/templates`)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  getHooks(){
    return this.http.get(`${environment.site_url}/api/pages/hooks`)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  post( data ){
    var data = data || {};
    return this.http.post(`${environment.site_url}/api/pages`, data)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  put( data, id ){

    var data = data || {};
    var id = id || null;

    return this.http.put(`${environment.site_url}/api/pages/${id}`, data)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  delete( id ) {
    return this.http.delete(`${environment.site_url}/api/pages/${id}`)
    .pipe(map((res: any) => {
      return res;
    }));
  }

}

