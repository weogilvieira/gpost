import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class TaxonomyService {

  constructor(private http: HttpClient) { }


  getAll( type: any, target = 'category'){
    return this.http.get(`${environment.site_url}/api/taxonomy/${type}/${target}`)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  get(id){
    return this.http.get(`${environment.site_url}/api/taxonomy/item/${id}`)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  post( type: any, target = 'category', formData ){
    return this.http.post(`${environment.site_url}/api/taxonomy/${type}/${target}`, formData)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  put( id, data ){

    var data = data || {};
    var id = id || null;

    return this.http.put(`${environment.site_url}/api/taxonomy/item/${id}`, data)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  delete( id ) {
    return this.http.delete(`${environment.site_url}/api/taxonomy/item/${id}`)
    .pipe(map((res: any) => {
      return res;
    }));
  }
}
