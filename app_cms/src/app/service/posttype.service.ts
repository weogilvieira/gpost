import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostTypeService {

  constructor(
    private http: HttpClient
  ) { }

  getAll(obj: any = {}){
    return this.http.get(`${environment.site_url}/api/posttype`, {
      params: obj
    })
    .pipe(map((res: any) => {
      return res;
    }));
  }

  get( id ){
    var id = id || null;
    return this.http.get(`${environment.site_url}/api/posttype/${id}`)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  post( data ){
    var data = data || {};
    return this.http.post(`${environment.site_url}/api/posttype`, data)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  put( data, id ){

    var data = data || {};
    var id = id || null;

    return this.http.put(`${environment.site_url}/api/posttype/${id}`, data)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  delete( id ) {
    return this.http.delete(`${environment.site_url}/api/posttype/${id}`)
    .pipe(map((res: any) => {
      return res;
    }));
  }


  getPosts(id: number = 0, obj: any = {}){
    return this.http.get(`${environment.site_url}/api/posttype/${id}/posts`, {
      params: obj
    })
    .pipe(map((res: any) => {
      return res;
    }));
  }

}

