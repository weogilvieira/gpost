import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class RelationshipService {

  constructor(private http: HttpClient) { }


  getAll( object_id ){
    return this.http.get(`${environment.site_url}/api/relationship/${object_id}`)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  post( object_id, data ){
    return this.http.post(`${environment.site_url}/api/relationship/${object_id}`, data)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  delete( id ) {
    return this.http.delete(`${environment.site_url}/api/relationship/${id}`)
    .pipe(map((res: any) => {
      return res;
    }));
  }
}
