import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MeService {

  constructor(
    private http: HttpClient
  ) { }

  get(){
    return this.http.get(`${environment.site_url}/api/me`)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  put( data ){
    return this.http.put(`${environment.site_url}/api/me`, data)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  updatePass( pass ){
    return this.http.put(`${environment.site_url}/api/me/password`, {
      password: pass
    })
    .pipe(
      map((res: any) => {
        return res;
      })
    );
  }

}

