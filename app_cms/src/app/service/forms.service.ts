import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Http, ResponseContentType } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class FormsService {

  constructor(
    private http: HttpClient,
    private httpClassic: Http
  ) { }

  getConfig(){
    return this.http.get(`${environment.site_url}/api/forms`)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  getAll(key, page = 1){
    return this.http.get(`${environment.site_url}/api/forms/${key}?page=${page}`)
    .pipe(map((res: any) => {
      return res;
    }));
  }


  delete( id ) {
    return this.http.delete(`${environment.site_url}/api/forms/${id}`)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  getCSV( key ) {
    var dt = new Date();
    var token = localStorage.getItem('token');

    return this.httpClassic.get(`${environment.site_url}/api/forms/${key}/csv?token=${token}`, {
      responseType: ResponseContentType.Blob
    }).pipe(map((res: any) => {
      return {
        filename: `${key}-${ dt.getTime() }.csv`,
        data: new Blob(["\ufeff", res._body])
      };
    }));
  }
}

