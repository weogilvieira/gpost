import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class MediaService {

  constructor(private http: HttpClient) { }


  getAll(obj, page = 1){
    var obj = obj || {};

    return this.http.get(`${environment.site_url}/api/media?page=${page}`, {
      params: obj
    })
    .pipe(map((res: any) => {
      return res;
    }));
  }

  get(id){
    return this.http.get(`${environment.site_url}/api/media/${id}`)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  post( formData ){
    var data = data || {};
    return this.http.post(`${environment.site_url}/api/media`, formData)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  delete( filename ) {
    return this.http.delete(`${environment.site_url}/api/media/${filename}`)
    .pipe(map((res: any) => {
      return res;
    }));
  }
}
