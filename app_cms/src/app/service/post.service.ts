import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(
    private http: HttpClient
  ) { }

  getAll(obj: any = {}, page: number = 1){
    return this.http.get(`${environment.site_url}/api/post?page=${page}`, {
      params: obj
    })
    .pipe(map((res: any) => {
      return res;
    }));
  }

  get( id ){
    var id = id || null;
    return this.http.get(`${environment.site_url}/api/post/${id}`)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  post( data ){
    var data = data || {};
    return this.http.post(`${environment.site_url}/api/post`, data)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  put( data, id ){

    var data = data || {};
    var id = id || null;

    return this.http.put(`${environment.site_url}/api/post/${id}`, data)
    .pipe(map((res: any) => {
      return res;
    }));
  }

  delete( id ) {
    return this.http.delete(`${environment.site_url}/api/post/${id}`)
    .pipe(map((res: any) => {
      return res;
    }));
  }

}

