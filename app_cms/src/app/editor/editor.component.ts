import { Component, OnInit, ElementRef, ContentChild, Injector, Input, Output, EventEmitter  } from '@angular/core';
import { environment } from '../../environments/environment';
import * as ClassicEditor from 'ckeditor5-build-gpost';
import { FormControl, FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {

  public CKEditor = ClassicEditor;

  // TheUploadAdapterPlugin(editor) {
  //   editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
  //     return new UploadAdapter(loader, `${environment.site_url}/api/media?token=${localStorage.getItem("token")}`);
  //   };
  // }

  ckconfig = {
    // extraPlugins: [ this.TheUploadAdapterPlugin ],
    alignment: {
      options: [ 'left', 'right', 'center' ]
    },
    language: 'pt-br',
    height: 480,
    simpleUpload: {
      uploadUrl: {
        url: `${environment.site_url}/api/media`,
        headers: {
          'x-access-token' : localStorage.getItem("token")
        }
      }
    }
    // toolbar: [
    //   "heading",
    //   "|",
    //   "bold",
    //   "italic",
    //   "link",
    //   "|",
    //   "bulletedList",
    //   "blockQuote",
    //   "insertTable",
    //   "|",
    //   "imageUpload",
    //   "mediaEmbed",
    //   "undo",
    //   "redo"
    // ]
  };


  @Input() disabled: boolean = false;
  @Input() target;

  @ContentChild('editor', { static: false }) elEditor:ElementRef;
  @Output() outputData = new EventEmitter();

  editor_form = new FormGroup({
    content: new FormControl('<div></div>')
  });

  constructor() {
  }

  ngOnInit() {
  // this.CKEditor.create( document.querySelector( '#editor' ), this.ckconfig )
  //   .then( editor => {
  //       console.log(editor)
  //   } )
  //   .catch( error => {
  //       console.error( error );
  //   } );

    this.target.valueChanges.subscribe( val => {

      if(val != this.editor_form.get('content').value){
        this.editor_form.patchValue({
          content: val
        });
      }
    });


    this.editor_form.get('content').valueChanges.subscribe( val => {
      this.outputData.emit(val);
    });

  }

}


// import { Http } from '@angular/http';

// class UploadAdapter {
//   loader;
//   url;
//   constructor(
//     loader,
//     url) {
//     this.loader = loader;
//     this.url = url;
//   }

//   upload() {
//     let _self = this;

//     return new Promise((resolve, reject) => {

//         let input = new FormData();
//         input.append('file', this.loader.file);

//         if(this.loader.file.size > 1000000 ){
//           reject("Este arquivo é maior que 1Mb.");
//           return;
//         }

//         var xhr = new XMLHttpRequest();
//         xhr.open('POST', this.url ,true);

//         xhr.onload = function() {
//           var res = JSON.parse(this.responseText);

//           if( res.message && !res.data ){
//             alert(res.message);
//             return reject();
//           }


//           resolve({
//             default: `${environment.site_url}/media/${res.data.filename}`
//           });

//         };

//         xhr.send(input);
//       })
//   }

//   abort() {
//     console.log('UploadAdapter abort');
//   }

// }
